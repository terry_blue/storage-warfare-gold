### What is this? ###

* The gold version of 24KT Studios Storage Warfare, base on the hit US TV show Storage Wars.
* Found here... https://play.google.com/store/apps/details?id=com.twentyfourktapps.storagewarfaregoldedition

### How do I get set up? ###

* Head over to the downloads page, download and install the apk on an any device running android 2.2
* To start, click start game, enter your name, head to the schedule section and pick an auction to attend.

### Notes ###

* Highscore server no longer operational, (owned by 24KT Studios)

### Questions? ###
* Send email to blueshroomuk@gmail.com