package com.twentyfourktapps.storagewarfaregoldedition;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class WrapupDialog extends Dialog {

	final private String TAG = "WrapupDialog";
	Context context;
	LockerUnit curLockerUnit;
	int curDisplayedItem;
	boolean repeat, isLastLocker;
	int tier, viewsLeft;
	String historyDate;
	
	final int MAX_MONTHS = 12;
	
	public WrapupDialog(Context pContext, int theme, int lockerID, boolean toRepeat, int lockerTier, int numOfViewsLeft, String hDate) {
		super(pContext, theme);
		context = pContext;
		repeat = toRepeat;
		tier = lockerTier;
		viewsLeft = numOfViewsLeft;
		historyDate = hDate;
		
		LockerUnitManager luM = new LockerUnitManager();
		curLockerUnit = luM.getLockerNumber(lockerID);
	}
	
	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wrapupdialog_layout);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setButtonListeners();
		curDisplayedItem = -1;
		isLastLocker = false;
		
		setNewDisplayedItem();
		
		//decide if this is the last viewing
		PlayerObject player = new PlayerObject(context);
		if(player.monthsPlayed >= MAX_MONTHS) isLastLocker = true;
		
	}
	
	private int getImageResourceId(String itemTitle) {
		if(itemTitle.contains("bamboofishingpole")) return R.drawable.bamboo_fishing_pole;
		else if(itemTitle.contains("barstool")) return R.drawable.barstool;
		else if(itemTitle.contains("baseballbat")) return R.drawable.baseball_bat;
		else if(itemTitle.contains("basketball")) return R.drawable.basketball;
		else if(itemTitle.contains("beachstuffbox")) return R.drawable.beach_stuff_box;
		else if(itemTitle.contains("beachball")) return R.drawable.beachball;
		else if(itemTitle.contains("benchpress")) return R.drawable.benchpress;
		else if(itemTitle.contains("bike")) return R.drawable.bike;
		else if(itemTitle.contains("boxinggloves")) return R.drawable.boxing_gloves;
		else if(itemTitle.contains("briefcase")) return R.drawable.briefcase;
		else if(itemTitle.contains("camera")) return R.drawable.camera;
		else if(itemTitle.contains("carrim")) return R.drawable.car_rim;
		else if(itemTitle.contains("chair")) return R.drawable.chair;
		else if(itemTitle.contains("chips")) return R.drawable.chips;
		else if(itemTitle.contains("civilwarsword")) return R.drawable.civilwar_sword;
		else if(itemTitle.contains("comicbookbox")) return R.drawable.comicbookbox;
		else if(itemTitle.contains("cooler")) return R.drawable.cooler;
		else if(itemTitle.contains("couch")) return R.drawable.couch;
		else if(itemTitle.contains("crate2")) return R.drawable.crate2;
		else if(itemTitle.contains("deepseafishingpole")) return R.drawable.deepsea_fishing_pole;
		else if(itemTitle.contains("fishtank")) return R.drawable.fish_tank;
		else if(itemTitle.contains("flatscreentv")) return R.drawable.flatscreen_tv;
		else if(itemTitle.contains("football")) return R.drawable.football;
		else if(itemTitle.contains("fragilebox")) return R.drawable.fragile_box;
		else if(itemTitle.contains("frame")) return R.drawable.frame;
		else if(itemTitle.contains("frame1")) return R.drawable.frame_1;
		else if(itemTitle.contains("frame2")) return R.drawable.frame_2;
		else if(itemTitle.contains("frame3")) return R.drawable.frame_3;
		else if(itemTitle.contains("fridge")) return R.drawable.fridge;
		else if(itemTitle.contains("gamesbox")) return R.drawable.games_box;
		else if(itemTitle.contains("gassign")) return R.drawable.gas_sign;
		else if(itemTitle.contains("godivapainting")) return R.drawable.godiva_painting;
		else if(itemTitle.contains("goldcoin")) return R.drawable.gold_coin;
		else if(itemTitle.contains("golfbag")) return R.drawable.golf_bag;
		else if(itemTitle.contains("guitar")) return R.drawable.guitar;
		else if(itemTitle.contains("gumballmachine")) return R.drawable.gumball_machine;
		else if(itemTitle.contains("lamp")) return R.drawable.lamp;
		else if(itemTitle.contains("laptop")) return R.drawable.laptop;
		else if(itemTitle.contains("loveseat")) return R.drawable.loveseat;
		else if(itemTitle.contains("mourningpainting")) return R.drawable.mourning_painting;
		else if(itemTitle.contains("neonbeer")) return R.drawable.neon_beer;
		else if(itemTitle.contains("neoncocktail")) return R.drawable.neon_cocktail;
		else if(itemTitle.contains("nightstand")) return R.drawable.nightstand;
		else if(itemTitle.contains("nightstand2")) return R.drawable.nightstand2;
		else if(itemTitle.contains("openbox")) return R.drawable.open_box;
		else if(itemTitle.contains("piggybank")) return R.drawable.piggybank;
		else if(itemTitle.contains("pokertable")) return R.drawable.poker_table;
		else if(itemTitle.contains("purse")) return R.drawable.purse;
		else if(itemTitle.contains("randombox")) return R.drawable.randombox;
		else if(itemTitle.contains("recordsbox")) return R.drawable.records_box;
		else if(itemTitle.contains("rifle")) return R.drawable.rifle;
		else if(itemTitle.contains("safe")) return R.drawable.safe;
		else if(itemTitle.contains("safe2")) return R.drawable.safe2;
		else if(itemTitle.contains("silvercoin")) return R.drawable.silver_coin;
		else if(itemTitle.contains("skateboard")) return R.drawable.skateboard;
		else if(itemTitle.contains("slotmachine")) return R.drawable.slot_machine;
		else if(itemTitle.contains("smallcrate")) return R.drawable.small_crate;
		else if(itemTitle.contains("sneakers")) return R.drawable.sneakers;
		else if(itemTitle.contains("sportscardsbox")) return R.drawable.sportscardsbox;
		else if(itemTitle.contains("stormpainting")) return R.drawable.storm_painting;
		else if(itemTitle.contains("stuffbox")) return R.drawable.stuffbox;
		else if(itemTitle.contains("subwoofer")) return R.drawable.subwoofer;
		else if(itemTitle.contains("sunglasses")) return R.drawable.sunglasses;
		else if(itemTitle.contains("sunlotionbox")) return R.drawable.sunlotion_box;
		else if(itemTitle.contains("surfboard")) return R.drawable.surfboard;
		else if(itemTitle.contains("sword")) return R.drawable.sword;
		else if(itemTitle.contains("tallbox2")) return R.drawable.tallbox2;
		else if(itemTitle.contains("teddybear")) return R.drawable.teddy_bear;
		else if(itemTitle.contains("tikibar")) return R.drawable.tiki_bar;
		else if(itemTitle.contains("tires")) return R.drawable.tires;
		else if(itemTitle.contains("toolbox")) return R.drawable.toolbox;
		else if(itemTitle.contains("trashbag")) return R.drawable.trash_bag;
		else if(itemTitle.contains("turntable")) return R.drawable.turntable;
		else if(itemTitle.contains("tv")) return R.drawable.tv;
		else if(itemTitle.contains("vase")) return R.drawable.vase;
		else if(itemTitle.contains("washingmachine")) return R.drawable.washing_machine;
		else {
			Log.e(TAG, "item pic not matched: " + itemTitle);
			return R.drawable.open_box;
		}
	}
	private void setButtonListeners() {
		final Button nextButton = (Button)findViewById(R.id.wrapupdialog_button_next);
		nextButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				setNewDisplayedItem();
				
			}
		});
		
		final Button finishButton = (Button)findViewById(R.id.wrapupdialog_button_skip);		
		finishButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				
				//Decide which screen should be next home, or new destination
				Intent mIntent;
				if (repeat) {
					mIntent = new Intent(context, DestinationScreen.class);
					mIntent.putExtra("tier", tier);
					mIntent.putExtra("attempts", viewsLeft);
					mIntent.putExtra("date", historyDate);
				} else if (isLastLocker){
					mIntent = new Intent(context, GameOverActivity.class);
				} else {
					mIntent = new Intent(context, HomeScreen.class);
				}
				
				mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				context.startActivity(mIntent);
				dismiss();
			}
		});
	}

	private void setNewDisplayedItem() {
		curDisplayedItem++;
		
		if(curDisplayedItem == (curLockerUnit.itemNameArrayList.size() - 1)) {
			final Button nextButton = (Button)findViewById(R.id.wrapupdialog_button_next);
			nextButton.setVisibility(View.INVISIBLE);
			final Button finishButton = (Button)findViewById(R.id.wrapupdialog_button_skip);
			finishButton.setText("Finish");
		}
		
		ImageView imageView = (ImageView)findViewById(R.id.wrapupdialog_imageview_central_image);
		TextView itemNameTextView = (TextView)findViewById(R.id.wrapupdialog_textview_itemname);		
		TextView priceTextView = (TextView)findViewById(R.id.wrapupdialog_textview_itemprice);	
		
		try {
			String priceString = "$" + Integer.toString(curLockerUnit.itemPriceArrayList.get(curDisplayedItem));
			priceTextView.setText(priceString);
			imageView.setImageResource(getImageResourceId(curLockerUnit.itemNameArrayList.get(curDisplayedItem)));
			
			StringBuilder sb = new StringBuilder();
			String temp = curLockerUnit.itemNameArrayList.get(curDisplayedItem);
			for(int i = 0; i < temp.length(); i++){
				if(temp.charAt(i) != '@') sb.append(temp.charAt(i));
				else break;
			}
			itemNameTextView.setText(sb.toString());		
			
		} catch(Exception e) {
			Log.e(TAG, "SetNewDisplayedItem" + e.toString());
		}
	}
	
}

