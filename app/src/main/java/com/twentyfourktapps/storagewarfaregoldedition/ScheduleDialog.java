package com.twentyfourktapps.storagewarfaregoldedition;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScheduleDialog extends Dialog {

	Context context;
	final String[] monthList = new String[] { "April", "May", "June", "July", "August", "September", "October", "November", "December", "January", "February", "March" };
	int playerCash, curMonthIndex, curTierIndex;
	String playerName, lowDate, medDate, highDate;
	final int MED_CASH_MIN = 800;
	final int HIGH_CASH_MIN = 1500;
	ArrayList<LinearLayout> dateLayouts;
	
	public ScheduleDialog(Context pContext, int theme) {
		super(pContext, theme);
		
		context = pContext;
		dateLayouts = new ArrayList<LinearLayout>();
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scheduledialog_layout);
		PlayerObject player = new PlayerObject(context);
		playerCash = player.currentScore;
		playerName = player.name;
		curMonthIndex = player.monthsPlayed;
		curTierIndex = player.daysPlayed;
		setDateLayoutIDs();
		setMetaData();
		setHeaderBar();
	}

	public void setScreenAnchorPoint(int width, int height) {
		int anchX = width / 8;
		int anchY = (int)((float)height / 9.5f);
		LinearLayout screenLayout = (LinearLayout)findViewById(R.id.scheduledialog_root_layout);
		screenLayout.setPadding(anchX, anchY, anchX, (anchY * 2));
	}
	
	
	private void setDateLayoutIDs() {
		final String TIER_INTENT_TAG = "tier";
		
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_01)); //0
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_02)); //1
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_03)); //2
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_04)); //3
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_05)); //4
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_06)); //5
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_07)); //6
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_08)); //7
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_09)); //8
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_10)); //9
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_11)); //10
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_12)); //11
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_13)); //12
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_14)); //13
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_15)); //14
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_16)); //15
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_17)); //16
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_18)); //17
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_19)); //18
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_20)); //19
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_21)); //20
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_22)); //21
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_23)); //22
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_24)); //23
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_25)); //24
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_26)); //25
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_27)); //26
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_28)); //27
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_29)); //28
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_30)); //29
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_31)); //30
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_32)); //31
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_33)); //32
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_34)); //33
		dateLayouts.add((LinearLayout)findViewById(R.id.calander_date_layout_35)); //34
		
		//Set the dates
		int lowPos = 1;
		int medPos = 10;
		int highPos = 20;
		
		switch (curMonthIndex) {
		case 0: { //April
			lowPos = 2;
			medPos = 12;
			highPos = 24;
			//Remove the unused dates from the calender
			dateLayouts.get(30).setVisibility(View.INVISIBLE);
			dateLayouts.get(31).setVisibility(View.INVISIBLE);
			dateLayouts.get(32).setVisibility(View.INVISIBLE);
			dateLayouts.get(33).setVisibility(View.INVISIBLE);
			dateLayouts.get(34).setVisibility(View.INVISIBLE);
			break;
		}
		case 1: { //May
			lowPos = 4;
			medPos = 19;
			highPos = 22;
			//Remove the unused dates from the calender
			dateLayouts.get(31).setVisibility(View.INVISIBLE);
			dateLayouts.get(32).setVisibility(View.INVISIBLE);
			dateLayouts.get(33).setVisibility(View.INVISIBLE);
			dateLayouts.get(34).setVisibility(View.INVISIBLE);
			break;
		}
		case 2: { //June
			lowPos = 3;
			medPos = 15;
			highPos = 25;
			//Remove the unused dates from the calender
			dateLayouts.get(30).setVisibility(View.INVISIBLE);
			dateLayouts.get(31).setVisibility(View.INVISIBLE);
			dateLayouts.get(32).setVisibility(View.INVISIBLE);
			dateLayouts.get(33).setVisibility(View.INVISIBLE);
			dateLayouts.get(34).setVisibility(View.INVISIBLE);
			break;
		}
		case 3: { //July
			lowPos = 5;
			medPos = 10;
			highPos = 29;
			//Remove the unused dates from the calender
			dateLayouts.get(31).setVisibility(View.INVISIBLE);
			dateLayouts.get(32).setVisibility(View.INVISIBLE);
			dateLayouts.get(33).setVisibility(View.INVISIBLE);
			dateLayouts.get(34).setVisibility(View.INVISIBLE);
			break;
		}
		case 4: { //August
			lowPos = 7;
			medPos = 11;
			highPos = 20;
			//Remove the unused dates from the calender
			dateLayouts.get(31).setVisibility(View.INVISIBLE);
			dateLayouts.get(32).setVisibility(View.INVISIBLE);
			dateLayouts.get(33).setVisibility(View.INVISIBLE);
			dateLayouts.get(34).setVisibility(View.INVISIBLE);
			break;
		}
		case 5: { //September
			lowPos = 6;
			medPos = 16;
			highPos = 26;
			//Remove the unused dates from the calender
			dateLayouts.get(30).setVisibility(View.INVISIBLE);
			dateLayouts.get(31).setVisibility(View.INVISIBLE);
			dateLayouts.get(32).setVisibility(View.INVISIBLE);
			dateLayouts.get(33).setVisibility(View.INVISIBLE);
			dateLayouts.get(34).setVisibility(View.INVISIBLE);
			break;
		}
			
			
		}
		
		lowDate = Integer.toString(lowPos) + " " + monthList[curMonthIndex];
		medDate = Integer.toString(medPos) + " " + monthList[curMonthIndex];
		highDate = Integer.toString(highPos) + " " + monthList[curMonthIndex];
		
		final PlayerObject player = new PlayerObject(context);
		
		for(int i = 0; i < dateLayouts.size(); i++) {
			TextView tv = new TextView(context);
			tv.setText(Integer.toString(i + 1));
			dateLayouts.get(i).addView(tv);
			
			if(i == lowPos) { //place low destination activator
				TextView setTitle = new TextView(context);
				setTitle.setText(R.string.storage_name_low);
				setTitle.setTextAppearance(context, R.style.scheduledialog_calander_datetext);
				TextView lockerCount = new TextView(context);
				lockerCount.setText("3 Lockers");
				lockerCount.setTextAppearance(context, R.style.scheduledialog_calander_datetext);
				
				if(curTierIndex < 1) {
					//TODO decide if it should be active
					dateLayouts.get(i).addView(setTitle);
					dateLayouts.get(i).addView(lockerCount);
					dateLayouts.get(i).setOnClickListener(new View.OnClickListener() {
						
						public void onClick(View v) {
							Intent mIntent = new Intent(context, DestinationScreen.class);
							mIntent.putExtra(TIER_INTENT_TAG, LockerUnit.TIER_LOW);		
							mIntent.putExtra("attempts", 3);
							mIntent.putExtra("date", lowDate);
							context.startActivity(mIntent);							
							player.setTier(1);
							
							dismiss();						
						}
					});
				}
				
				
				
				
			} else if (i == medPos) { //place med destination activator
				TextView setTitle = new TextView(context);
				setTitle.setText(R.string.storage_name_med);
				setTitle.setTextAppearance(context, R.style.scheduledialog_calander_datetext);
				TextView lockerCount = new TextView(context);
				lockerCount.setText("3 Lockers");
				lockerCount.setTextAppearance(context, R.style.scheduledialog_calander_datetext);
				
				if(curTierIndex < 2) {
					//TODO decide if it should be active
					dateLayouts.get(i).addView(setTitle);
					dateLayouts.get(i).addView(lockerCount);
					dateLayouts.get(i).setOnClickListener(new View.OnClickListener() {
						
						public void onClick(View v) {
							Intent mIntent = new Intent(context, DestinationScreen.class);
							mIntent.putExtra(TIER_INTENT_TAG, LockerUnit.TIER_MED);
							mIntent.putExtra("attempts", 3);
							mIntent.putExtra("date", medDate);
							context.startActivity(mIntent);
							player.setTier(2);
							
							dismiss();						
						}
					});
				}
				
				
			} else if (i == highPos) { //place high destination activator
				TextView setTitle = new TextView(context);
				setTitle.setText(R.string.storage_name_high);
				setTitle.setTextAppearance(context, R.style.scheduledialog_calander_datetext);
				TextView lockerCount = new TextView(context);
				lockerCount.setText("3 Lockers");
				lockerCount.setTextAppearance(context, R.style.scheduledialog_calander_datetext);
				
				dateLayouts.get(i).addView(setTitle);
				dateLayouts.get(i).addView(lockerCount);
				dateLayouts.get(i).setOnClickListener(new View.OnClickListener() {
					
					public void onClick(View v) {
						Intent mIntent = new Intent(context, DestinationScreen.class);
						mIntent.putExtra(TIER_INTENT_TAG, LockerUnit.TIER_HIGH);
						mIntent.putExtra("attempts", 3);
						mIntent.putExtra("date", highDate);
						context.startActivity(mIntent);
						//player.setTier(3);
						player.incrementMonth();
						
						dismiss();
						
					}
				});
			}
		}
	}
	private void setMetaData() {
		TextView monthTV = (TextView)findViewById(R.id.scheduledialog_textview_month);
		TextView monthsPlayedTV = (TextView)findViewById(R.id.scheduledialog_textview_months_played);
		monthTV.setText(monthList[curMonthIndex]);
		monthsPlayedTV.setText(Integer.toString(curMonthIndex + 1));
	}
	private void setHeaderBar() {
		TextView nameTV = (TextView)findViewById(R.id.scheduledialog_textview_playername);
		TextView cashTV = (TextView)findViewById(R.id.scheduledialog_textview_playerscash);
		
		nameTV.setText(playerName);
		cashTV.setText(Integer.toString(playerCash));
		
		final Button skipMonthButton = (Button)findViewById(R.id.scheduledialog_button_advance_month);
		
		skipMonthButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				final PlayerObject player = new PlayerObject(context);
				
				if(curMonthIndex >= (monthList.length - 1)) {
					Intent mIntent = new Intent(context, GameOverActivity.class);
					mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					context.startActivity(mIntent);
				} else {					
					player.incrementMonth();					
				}
				
				dismiss();
			}
		});
	}
}

