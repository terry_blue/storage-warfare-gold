package com.twentyfourktapps.storagewarfaregoldedition;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.twentyfourktapps.storagewarfaregoldedition.LockerUnitManager.HistoryObject;


public class MainWrapupDialog extends Dialog {

	Context context;
	TextView profit_lossTV, figureTV, playerNameTV, locationTV, dateTV;
	Button okButton;
	
	public MainWrapupDialog(Context pContext, int theme) {
		super(pContext, theme);
		context = pContext;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.main_wrapup_dialog);
		setViews();
		setContent();
	}
	
	public void setScreenAnchorPoint(int width, int height) {
		int anchX = width / 8;
		int anchY = (int)((float)height / 9.5f);
		LinearLayout screenLayout = (LinearLayout)findViewById(R.id.itemscollectiondialog_layout_root);
		screenLayout.setPadding(anchX, anchY, anchX, (anchY * 2));
	}
	
	private void setViews() {
		profit_lossTV = (TextView)findViewById(R.id.main_wrapup_dialog_textview_profitloss);
		figureTV = (TextView)findViewById(R.id.main_wrapup_dialog_textview_figure);
		playerNameTV = (TextView)findViewById(R.id.main_wrapup_dialog_textview_playername);
		dateTV = (TextView)findViewById(R.id.main_wrapup_dialog_textview_date); 
		locationTV = (TextView)findViewById(R.id.main_wrapup_dialog_textview_location_name);
		
		okButton = (Button)findViewById(R.id.main_wrapup_dialog_button_ok);
		okButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dismiss();				
			}
		});
	}
	private void setContent() {
		PlayerObject player = new PlayerObject(context);
		playerNameTV.setText(player.name);
		DatabaseInterface DI = new DatabaseInterface(context);
		ArrayList<HistoryObject> history = DI.getHistory();
		
		if(history != null) {
			//Date
			String dateString = history.get(history.size() - 1).date;
			dateTV.setText(dateString);
			
			int profit = 0;
			for(int i = history.size() - 1; i >= 0; i--) {
				if(history.get(i).date.equals(dateString)) {
					profit += Integer.parseInt(history.get(i).profit);
				}
			}
			
			String profitLossString = "";
			if(profit >= 0) {
				profitLossString = "profit";
			} else {
				profitLossString = "loss";
				profit *= -1;
			}
			
			String figureString = Integer.toString(profit);
			
			profit_lossTV.setText(profitLossString);
			figureTV.setText(figureString);
			
			String locationString = "to the auctions";
			String locationNumString = history.get(history.size() - 1).title;
			//Log.e("Main Wrapup dialog", "locker number = " + locationNumString);
			String[] highTierArray = new String[] { "157", "161", "164", "166", "173", "177", "178", "197", "198", "199", "200", "201", "202", "203", "204" };
			String[] medTierArray = new String[] { "150", "154", "155", "158", "159", "163", "165", "168", "170", "171", "172", "174", "175", "176", "182", "183", "184", "185" };
			String[] lowTierArray = new String[] { "151", "152", "153", "156", "160", "162", "167", "169", "179", "180", "181", "186", "187", "188", "189", "190", "191", "192", "193", "194", "195", "196" };
			
			for (int i = 0; i < highTierArray.length; i++) {
				if(locationNumString.contains(highTierArray[i])) {
					locationString = "Top Shelf Storage";
				}
			}
			for (int i = 0; i < medTierArray.length; i++) {
				if(locationNumString.contains(medTierArray[i])) {
					locationString = "Eastbound Storage";
				}
			}
			for (int i = 0; i < lowTierArray.length; i++) {
				if(locationString.contains(lowTierArray[i])) {
					locationString = "Stack \'N\' Storage";
				}
			}
			locationTV.setText(locationString);
			
		} else {
			dismiss();
		}
		
	}
}

