package com.twentyfourktapps.storagewarfaregoldedition;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.twentyfourktapps.storagewarfaregoldedition.LockerUnitManager.HistoryObject;


public class ItemsCollectionDialog extends Dialog {
	
	private String[] itemNames = { "Bag / Purse", "Bamboo Fishing Pole", "Baseball Bat", "Basketball", "Beachball", "Benchpress", "Boxing Gloves", "Briefcase", "Camera", "Civil War Sword", "Cooler", "Couch", "Deep Sea Fishing Pole", "DJ Turntable", "Fish Tank", "Flat Screen TV", "Football", "Gas Sign", "Gold Coin", "Golf Bag", "Guitar", "Gumball Machine", "Lamp", "Laptop", "Loveseat", "Mountain Bike", "Neon Beer Sign", "Neon Cocktail Sign", "Old TV", "Picture 1", "Picture 2", "Picture 3", "Piggy Bank", "Poker Chips", "Poker Table", "Refrigerator", "Rifle", "Silver Coin", "Skateboard", "Slot Machine", "Sneakers", "Subwoofer", "Sunglasses", "Surfboard", "Sword", "Teddy Bear", "Tiki Bar", " Tires", "Toolbox", "Vase", "Washing Machine" };
	private String[] itemFlags = { "purse", "bamboofishingpole", "baseballbat", "basketball", "beachball", "benchpress", "boxinggloves", "briefcase", "camera", "civilwarsword", "cooler", "couch", "deepseafishingpole", "turntable", "fishtank", "flatscreentv", "football", "gassign", "goldcoin", "golfbag", "guitar", "gumballmachine", "lamp", "laptop", "loveseat", "bike", "neonbeer", "neoncocktail", "tv", "frame", "frame2", "frame3", "piggybank","chips", "pokertable", "frigde", "rifle", "silvercoin", "skateboard", "slotmachine", "sneakers", "subwoofer", "sunglasses", "surfboard", "sword", "teddybear", "tikibar", "tires", "toolbox", "vase", "washingmachine" };
	private Integer[] resourceIDs = { R.drawable.purse, R.drawable.bamboo_fishing_pole, R.drawable.baseball_bat, R.drawable.basketball, R.drawable.beachball, R.drawable.benchpress, R.drawable.boxing_gloves, R.drawable.briefcase, R.drawable.camera, R.drawable.civilwar_sword, R.drawable.cooler, R.drawable.couch, R.drawable.deepsea_fishing_pole, R.drawable.turntable, R.drawable.fish_tank, R.drawable.flatscreen_tv, R.drawable.football, R.drawable.gas_sign, R.drawable.gold_coin, R.drawable.golf_bag, R.drawable.guitar, R.drawable.gumball_machine, R.drawable.lamp, R.drawable.laptop, R.drawable.loveseat, R.drawable.bike, R.drawable.neon_beer, R.drawable.neon_cocktail, R.drawable.tv, R.drawable.frame, R.drawable.frame_2, R.drawable.frame_3, R.drawable.piggybank, R.drawable.chips, R.drawable.poker_table, R.drawable.fridge, R.drawable.rifle, R.drawable.silver_coin, R.drawable.skateboard, R.drawable.slot_machine, R.drawable.sneakers, R.drawable.subwoofer, R.drawable.sunglasses, R.drawable.surfboard, R.drawable.sword, R.drawable.teddy_bear,R.drawable.tiki_bar, R.drawable.tires, R.drawable.toolbox, R.drawable.vase, R.drawable.washing_machine };
	Context context;
	boolean[] foundFlags;
	Bitmap lockedBMP;

	public ItemsCollectionDialog(Context pContext, int theme) {
		super(pContext, theme);
		context = pContext;
		foundFlags = new boolean[itemNames.length];
		for(int i = 0; i < itemNames.length; i++) {
			foundFlags[i] = false;
		}
		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.itemscollectiondialog_layout);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		lockedBMP = BitmapFactory.decodeResource(context.getResources(), R.drawable.unknown_item);		
		
		//Cross reference items
		DatabaseInterface DI = new DatabaseInterface(context);
		ArrayList<HistoryObject> historyObjects = DI.getHistory();
		
		ArrayList<String> lockerList = new ArrayList<String>();
		
		for(int i = 0; i < historyObjects.size(); i++) {
			if(!historyObjects.get(i).in.equals("0")) {
				lockerList.add(historyObjects.get(i).title);
			}
		}
		
		//Log.e("Locker List", Integer.toString(lockerList.size()));
		
		LockerUnitManager lUM = new LockerUnitManager();
		ArrayList<LockerUnit> allLockerUnits = lUM.allLockerUnits;
		ArrayList<LockerUnit> lockerUnits = new ArrayList<LockerUnit>();
		
		
		
		for(int i = 0; i < allLockerUnits.size(); i++) {
			for(int a = 0; a < lockerList.size(); a++) {
				if(allLockerUnits.get(i).lockerTitle.equals(lockerList.get(a))) {
					lockerUnits.add(allLockerUnits.get(i));
				}
			}
			
		}
		
		//Log.e("Locker Units", lockerUnits.toString());
		
		for(int i = 0; i < lockerUnits.size(); i++) {
			ArrayList<String> lockerItemList = lockerUnits.get(i).itemNameArrayList;
			for(int b = 0; b < lockerItemList.size(); b++) {
				for(int c = 0; c < itemFlags.length; c++) {
					if(lockerItemList.get(b).contains(itemFlags[c])) {
						foundFlags[c] = true;					
					} 
				}
			}
		}
		createTable();
	}

	private void createTable() {
		LinearLayout rootLayout = (LinearLayout)findViewById(R.id.itemscollectiondialog_layout_itemcollection_root);
		rootLayout.removeAllViews();
		
		for(int i = 0; i < foundFlags.length; i++) {
			LinearLayout imageLayout = new LinearLayout(context);
			imageLayout.setGravity(Gravity.CENTER);
			
			ImageView image = new ImageView(context);
			image.setAdjustViewBounds(true);
			TextView itemTitleTV = new TextView(context);
			itemTitleTV.setTextAppearance(context, R.style.bankdialog_statement_text);
			itemTitleTV.setGravity(Gravity.CENTER_VERTICAL);
			itemTitleTV.setPadding(40, 10, 0, 0);
			
			if(foundFlags[i]) { 
				image.setImageResource(resourceIDs[i]);
				image.setMaxWidth(150);
				image.setMinimumWidth(150);
				imageLayout.addView(image);
				itemTitleTV.setText(itemNames[i]);
			}
			else { 
				image.setImageBitmap(lockedBMP);
				image.setMaxWidth(150);
				image.setMinimumWidth(150);
				imageLayout.addView(image);
				itemTitleTV.setText("LOCKED");
			}
			
			
			//Set the text and image to a layout and add that layout to the scrollview root.
			LinearLayout rowLayout = new LinearLayout(context);
			rowLayout.addView(imageLayout, new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, 2)); rowLayout.addView(itemTitleTV, new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, 1));
			
			
			rootLayout.addView(rowLayout);			
		}
	}
	
	public void setScreenAnchorPoint(int width, int height) {
		int anchX = width / 8;
		int anchY = (int)((float)height / 9.5f);
		LinearLayout screenLayout = (LinearLayout)findViewById(R.id.itemscollectiondialog_layout_root);
		screenLayout.setPadding(anchX, anchY, anchX, (anchY * 2));
	}

	public int getInt(int value) {
		return value;
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		dismiss();
	}
}

