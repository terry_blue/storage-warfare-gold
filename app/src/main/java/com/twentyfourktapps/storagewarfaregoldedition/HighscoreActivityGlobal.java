package com.twentyfourktapps.storagewarfaregoldedition;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;


public class HighscoreActivityGlobal extends HighscoreAbstract{

	ArrayList<HighscoreObject> highscoreObjects;
	LinearLayout table;
	
	String rankString;
	String scoreString;
	
	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.highscore_activity_global_layout);
		table = new LinearLayout(context);
		table.setOrientation(LinearLayout.VERTICAL);
		
		try {
			Receiver receiver = new Receiver();
			receiver.start();
		} catch (Exception e) {
			Log.e("Highscore Actvity Global", e.toString());
			TextView errorMessageTV = new TextView(context);
			errorMessageTV.setText("An error occured please check your data/internet connection");
			table.addView(errorMessageTV);
		}
		
	}
	
	Handler tableBuilt = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if(msg.obj.equals("build_complete")) {
				//remove the progress spinner and insert the table
				LinearLayout tableRootLayout = (LinearLayout)findViewById(R.id.highscore_activity_global_linearlayout_table);
				tableRootLayout.removeAllViews();
				tableRootLayout.addView(table);
				createTable();			
				
			} else if (msg.obj.equals("build_error")) {
				//Display an error message
				
			}
		}
	};
	
	private void createTable() {
		//get the width of the pos and country columns
		TextView positionTV = (TextView)findViewById(R.id.highscore_activity_global_textview_position_title);
		int posColumnWidth = positionTV.getWidth();
		TextView countryTV = (TextView)findViewById(R.id.highscore_activity_global_textview_country_title);
		int countryColumnWidth = countryTV.getWidth();
		
		
		//Set player specific data
		TextView rankTV = (TextView)findViewById(R.id.highscore_activity_global_textview_rank);
		TextView scoreTV = (TextView)findViewById(R.id.highscore_activity_global_textview_score);		
		rankTV.setText(rankString);
		scoreTV.setText(scoreString);
		
		//Set the highscore table data
		for(int i = 0; i < highscoreObjects.size(); i++) {
			LinearLayout row = new LinearLayout(context);
			LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, 1);
			
			TextView posTV = new TextView(context);
			posTV.setText(Integer.toString(i + 1));
			posTV.setWidth(posColumnWidth);
			row.addView(posTV);
			
			TextView nameTV = new TextView(context);
			nameTV.setText(highscoreObjects.get(i).name);
			row.addView(nameTV, params);
			
			TextView cashInTV = new TextView(context);
			cashInTV.setText(Integer.toString(highscoreObjects.get(i).totalIn));
			row.addView(cashInTV, params);
			
			TextView cashOutTV = new TextView(context);
			cashOutTV.setText(Integer.toString(highscoreObjects.get(i).totalOut));
			row.addView(cashOutTV, params);
			
			TextView endScoreTV = new TextView(context);
			endScoreTV.setText(Integer.toString(highscoreObjects.get(i).endScore));
			row.addView(endScoreTV, params);
			
			TextView countryPlaceHolder = new TextView(context);
			countryPlaceHolder.setText("");
			countryPlaceHolder.setWidth(countryColumnWidth);
			row.addView(countryPlaceHolder);
			
			table.addView(row);
		}
		
	}
	
	
	private class Receiver extends Thread {
		@Override
		public void run() {
			//Get the users unique IMEI
			//TelephonyManager manager = (TelephonyManager)context.getSystemService(context.TELEPHONY_SERVICE);
			UUID_Manager uuid_m = new UUID_Manager(context);
			
			
			GlobalHighscoreReciever ghsr = new GlobalHighscoreReciever(context, uuid_m.getUUID(), "global", "global");
			
			highscoreObjects = ghsr.getHighscoreObjects();
			rankString = ghsr.getUsersRank();
			scoreString = ghsr.getUsersHighestScore();
			
			if(rankString.equals("-1")) rankString = "Unranked";
			if(scoreString.equals("-1")) scoreString = "None";
			
			//Send message back to original thread
			Message msg = new Message();
			msg.obj = "build_complete";
			tableBuilt.sendMessage(msg);
		}
		
		
	}
	
	@Override
	public void onBackPressed() {
		Intent mIntent = new Intent(context, TitleScreen.class);
		mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(mIntent);	
	}
	
}

