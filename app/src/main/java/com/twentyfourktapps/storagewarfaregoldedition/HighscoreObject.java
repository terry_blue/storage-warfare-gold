package com.twentyfourktapps.storagewarfaregoldedition;

public class HighscoreObject {

	int globalRecID, localRecID, globalUserID, totalIn, totalOut, endScore, biggestLoss, biggestWin, winStreak, lossStreak;
	String name;
	long date;
		
	public HighscoreObject() {
		
	}
	
	public void setData(int globalRecId, int globalUserId, int localRecID, int totalCashIn, int totalCashOut, int biggestCashLoss, int biggestCashWin, int winningStreak, int losingStreak, String uName, long lDate) {
		globalRecID = globalRecId;
		globalUserID = globalUserId;
		totalIn = totalCashIn;
		totalOut = totalCashOut;
		biggestLoss = biggestCashLoss;
		biggestWin = biggestCashWin;
		winStreak = winningStreak;
		lossStreak = losingStreak;
		name = uName;		
		date = lDate;
		
		endScore = totalCashIn - totalCashOut;
	}
}
