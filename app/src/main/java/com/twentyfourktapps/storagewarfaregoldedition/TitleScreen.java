package com.twentyfourktapps.storagewarfaregoldedition;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class TitleScreen extends Activity implements LocationListener{

	Context context;
	boolean toContinueGame;
	GameSounds gameSounds;
	
	LocationManager locationManager;
	Geocoder geocoder;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState);
		setContentView(R.layout.titlescreen_layout);
		context = this;
		
		locationManager = (LocationManager)context.getSystemService(LOCATION_SERVICE);
		geocoder = new Geocoder(context);
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100000, 100000, this);
		Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		if(location != null) {
			this.onLocationChanged(location);
		}
		
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		gameSounds = new GameSounds(context);
		
		setButtonListeners();
	}
	
	private void setButtonListeners() {
		
		Button startButton = (Button)findViewById(R.id.title_button_start);
		ImageButton hiscoreButton = (ImageButton)findViewById(R.id.title_button_highscores);
		Button howtoPlayButton = (Button)findViewById(R.id.title_button_howtoplay);
		//Button aboutButton = (Button)findViewById(R.id.title_button_about);
		Button quitButton = (Button)findViewById(R.id.title_button_quit);
		
		startButton.setOnClickListener(new View.OnClickListener() {
			//Start Button Click
			public void onClick(View v) {
				//Determine if continue game or new game dialog should be shown
				PlayerObject playerObject = new PlayerObject(context);
				gameSounds.playClick();
				
				if(playerObject.isActive) {
					//start the continue game dialog
					ContinueGameDialog continueDialog = new ContinueGameDialog(context, R.style.continue_game_layout);
					continueDialog.show();
					continueDialog.setName(playerObject.name);
					
				} else { //start the new game dialog
					NewGameDialog newGameDialog = new NewGameDialog(context, R.style.new_game_layout);
					newGameDialog.show();
				}
				
				playerObject.close();
			}
		});
		
		hiscoreButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				gameSounds.playClick();
				startActivity(new Intent(context, HighscoreScreen.class));
				
			}
		});
		
		howtoPlayButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				startActivity(new Intent(context, InstructionsActivity.class));
			}
		});
		
		quitButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				onBackPressed();				
			}
		});
	}

	@Override
	public void onLocationChanged(Location location) {
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onBackPressed() {
		TimeSender timeSender = new TimeSender();
		timeSender.start();
		super.onBackPressed();
	}

	@Override
	public void onPause() {
		super.onPause();
		locationManager.removeUpdates(this);
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}
	
	private class TimeSender extends Thread {
		
		@Override
		public void run() {
			final PackageManager pm = context.getPackageManager();
	        ApplicationInfo ai;
	        try {
	        	ai = pm.getApplicationInfo("com.twentyfourktapps.storagewarfaregoldedition", 0);
	        } catch (Exception e) {
	        	ai = null;
	        	Log.e("Splash", "getApplicationInfo error: " + e.toString());
	        }        
	        final String APPLICATION_NAME = (String)(ai != null ? pm.getApplicationLabel(ai) : "(unknown)");
			TimeChecker timeChecker = new TimeChecker(context, APPLICATION_NAME);
			timeChecker.setEndTime();
			timeChecker.setDataForSever();
			timeChecker.sendDataToServer();
		}
	}
}
