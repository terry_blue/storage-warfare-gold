package com.twentyfourktapps.storagewarfaregoldedition;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

public class HighscoreScreen extends TabActivity {

	Context context;
	String countryString = "";

	
	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		setContentView(R.layout.highscorescreen_layout);
		
		
	    TabHost tabHost = getTabHost();
	    TabHost.TabSpec spec;
	    Intent intent;
	    
	    //Local Tab
	    intent = new Intent(context, HighscoreActivityLocal.class);
	    spec = tabHost.newTabSpec("local").setIndicator("Local").setContent(intent);
	    tabHost.addTab(spec);
	    
	    //Regional Tab
	    intent = new Intent(context, HighscoreActivityRegional.class);
	    spec = tabHost.newTabSpec("regional").setIndicator("Regional").setContent(intent);
	    tabHost.addTab(spec);
	    
	    //Global Tab
	    intent = new Intent(context, HighscoreActivityGlobal.class);
	    spec = tabHost.newTabSpec("global").setIndicator("Global").setContent(intent);
	    tabHost.addTab(spec);
	    
	    tabHost.setCurrentTab(0);
	}

	@Override
	public void onBackPressed() {
		Intent mIntent = new Intent(context, TitleScreen.class);
		mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(mIntent);	
	}
	
}
