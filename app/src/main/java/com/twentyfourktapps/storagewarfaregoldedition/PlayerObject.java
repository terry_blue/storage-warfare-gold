package com.twentyfourktapps.storagewarfaregoldedition;

import android.content.Context;


//File used as an interface to the playerDB
public class PlayerObject {

	Context context;
	DatabaseInterface DI;
	String name = "";
	final static int START_CASH = 2000;
	int currentScore = 0;
	//int completedUnits = 0;
	int monthsPlayed = 0;
	int daysPlayed = 0;
	boolean isActive;
	
	public PlayerObject(Context pContext) {
		context = pContext;
		DI = new DatabaseInterface(context);
		LoadFields();		
	}
	
	
	public void close() {
		DI.close();	
	}
	
	//New Game Player
	public boolean CreateNew(String newName) {
		if (isActive) return false;
		else {
			currentScore = START_CASH;
			name = newName;
			isActive = true;
			monthsPlayed = 0;
			daysPlayed = 0;
			SaveFields();
			return true;
		}		
	}
	
	public void incrementMonth() {
		monthsPlayed++;
		daysPlayed = 0;
		
		SaveFields();
	}
	public void setTier(int level) {
		daysPlayed = level;
		
		SaveFields();
		
	}
	
	//Reset player
	public void Reset() {
		currentScore = START_CASH;
		monthsPlayed = 0;
		daysPlayed = 0;
		name = "Spencer";
		isActive = false;
		
		SaveFields();
	}
	
	//Load player values
	public void LoadFields() {		
		PlayerStruct struct = DI.getPlayerStruct();
		
		currentScore = struct.cash;
		name = struct.name;
		isActive = struct.isActive;
		monthsPlayed = struct.viewedMonths;
		daysPlayed = struct.viewedTiers;
		
		DI.close();
	}
	//Save player values
	public void SaveFields() {
		DI = new DatabaseInterface(context);
		
		DI.updatePlayersRow(name, currentScore, isActive, Integer.toString(monthsPlayed), Integer.toString(daysPlayed));
		
		DI.close();
	}	
}
