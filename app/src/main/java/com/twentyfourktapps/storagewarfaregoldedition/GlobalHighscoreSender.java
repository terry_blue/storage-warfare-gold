package com.twentyfourktapps.storagewarfaregoldedition;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.os.Build;
import android.util.Log;

public class GlobalHighscoreSender {

	HttpClient httpClient;
	HttpPost httpPost;
	final String postURLScript = "http://drunkzombies.com/Android/Highscores/Games/store_storagewarfare_complete.php";
	ArrayList<NameValuePair> nameValuePairs;
	private boolean isDataSet;
	
	public GlobalHighscoreSender() {
		httpClient = new DefaultHttpClient();
		httpPost = new HttpPost(postURLScript);
		nameValuePairs = new ArrayList<NameValuePair>(2);
		isDataSet = false;
	}
	
	public void setFormData(String UUID, String country, HighscoreObject ho) {
		nameValuePairs.add(new BasicNameValuePair("uuid", UUID));
		nameValuePairs.add(new BasicNameValuePair("name", ho.name));
		nameValuePairs.add(new BasicNameValuePair("end_score", Integer.toString(ho.endScore)));
		nameValuePairs.add(new BasicNameValuePair("total_in", Integer.toString(ho.totalIn)));
		nameValuePairs.add(new BasicNameValuePair("total_out", Integer.toString(ho.totalOut)));
		nameValuePairs.add(new BasicNameValuePair("biggest_win", Integer.toString(ho.biggestWin)));
		nameValuePairs.add(new BasicNameValuePair("biggest_loss", Integer.toString(ho.biggestLoss)));
		nameValuePairs.add(new BasicNameValuePair("win_streak", Integer.toString(ho.winStreak)));
		nameValuePairs.add(new BasicNameValuePair("loss_streak", Integer.toString(ho.lossStreak)));
		nameValuePairs.add(new BasicNameValuePair("country", country));
		nameValuePairs.add(new BasicNameValuePair("os_version", Build.VERSION.RELEASE));
		
		isDataSet = true;
	}
	public void sendFormData() {
		if(isDataSet) {
			try {
				httpClient = new DefaultHttpClient();
				httpPost = new HttpPost(postURLScript);
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				httpClient.execute(httpPost);				
			} catch (Exception e) {
				Log.e("SEND HIGHSCORE FORM DATA ERROR", e.toString());
			}
		} else {
			Log.e("SEND HIGHSCORE DATA ERROR", "Data not set");
		}
	}

	

}

