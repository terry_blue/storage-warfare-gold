package com.twentyfourktapps.storagewarfaregoldedition;

import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.preference.PreferenceManager;

public abstract class SoundManager {
	Context context;
	public SoundPool soundPool;
	public HashMap<Integer, Integer> soundPoolMap;
	boolean isSoundEnabled;
	
	public SoundManager(Context pContext) {
		context = pContext;
		soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
		soundPoolMap = new HashMap<Integer, Integer>();
	
		//check to see if the sound is enabled
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		isSoundEnabled = sp.getBoolean("soundEnabled", true);
	}
	
	
	public void initializeSound(int soundID,  int rawId, int priority) {
		soundPoolMap.put(soundID, soundPool.load(context, rawId, priority));
	}
	
	public void playSound(int sound) {
		if (isSoundEnabled) {
			/* Updated: The next 4 lines calculate the current volume in a scale of 0.0 to 1.0 */
		    AudioManager mgr = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		    float streamVolumeCurrent = mgr.getStreamVolume(AudioManager.STREAM_MUSIC);
		    float streamVolumeMax = mgr.getStreamMaxVolume(AudioManager.STREAM_MUSIC);    
		    float volume = streamVolumeCurrent / streamVolumeMax;
		    
		    /* Play the sound with the correct volume */
		    soundPool.play(soundPoolMap.get(sound), volume, volume, 1, 0, 1f);
		}
	}
	
	public void warmUpSound(int sound) {
		soundPool.play(sound, 0, 0, 1, 0, 1f);
	}
}

