package com.twentyfourktapps.storagewarfaregoldedition;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.WindowManager;

public class PreDestinationDialog extends Dialog{

	Context context;
	GameSounds soundManager;
	
	public PreDestinationDialog(Context pContext, int theme) {
		super(pContext, theme);
		context = pContext;
		soundManager = new GameSounds(context);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.predestinationdialog_layout);
		
	
	}
	
}
