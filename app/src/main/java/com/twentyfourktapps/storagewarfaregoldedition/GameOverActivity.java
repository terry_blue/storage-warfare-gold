package com.twentyfourktapps.storagewarfaregoldedition;

import java.util.ArrayList;
import java.util.List;

import com.twentyfourktapps.storagewarfaregoldedition.LockerUnitManager.HistoryObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class GameOverActivity extends Activity implements LocationListener{

	Context context;
	HighscoreObject highscoreObject;
	String countryString = "";
	
	LocationManager locationManager;
	Geocoder geocoder;
	
	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gameover_layout);
		context = this;
		
		locationManager = (LocationManager)context.getSystemService(LOCATION_SERVICE);
		geocoder = new Geocoder(context);
		
		try {
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000000, 10000000, this);
			Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			if(location != null) {
				
				this.onLocationChanged(location);
			}
		} catch (Exception e) {
			Log.e("Location manager error", e.toString());
		}
		
		
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		
		Button titleButton = (Button)findViewById(R.id.gameover_button_title);
		titleButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				//reset the player and delete the history
				resetPlayerData();
				Intent mIntent = new Intent(context, TitleScreen.class);
				mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(mIntent);				
			}
		});
		Button hiscoreButton = (Button)findViewById(R.id.gameover_button_highscores);
		hiscoreButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				resetPlayerData();
				Intent mIntent = new Intent(context, HighscoreScreen.class);
				mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(mIntent);	
			}
		});
		
		//**Save the highscores**//
		setHighscoreObject();
		saveLocalHighscore();
		saveGlobalHighscore();
	}
	
	private void setHighscoreObject() {
		DatabaseInterface DI = new DatabaseInterface(context);
		String localName = DI.getPlayerStruct().name;
		ArrayList<HistoryObject> history = DI.getHistory();
		
		int totalIn = 0;
		int totalOut = 0;
		int biggestWin = 0;
		int biggestLoss = 0;
		int winStreak = 0;
		int lossStreak = 0;
		int tempLossStreak = 0;
		int tempWinStreak = 0;
		
		long localDate = System.currentTimeMillis();
		
		for(int i = 0; i < history.size(); i++) {
			totalIn += Integer.parseInt(history.get(i).in);
			totalOut += Integer.parseInt(history.get(i).out);
			int profit = Integer.parseInt(history.get(i).in) - Integer.parseInt(history.get(i).out);
			
			if(profit > biggestWin) biggestWin = profit;
			if(profit < biggestLoss) biggestLoss = profit;
			
			//work out win streak
			
			if(Integer.parseInt(history.get(i).in) <= 0) tempLossStreak++;
			else {
				if(tempLossStreak > lossStreak) lossStreak = getInt(tempLossStreak);
				tempLossStreak = 0;				
			}
			
			if(Integer.parseInt(history.get(i).out) > 0) tempWinStreak++;
			else {
				if(tempWinStreak > winStreak) winStreak = getInt(tempWinStreak);
				tempWinStreak = 0;				
			}
		}
		
		
		
		highscoreObject = new HighscoreObject();
		highscoreObject.setData(0, 0, 0, totalIn, totalOut, biggestLoss, biggestWin, winStreak, lossStreak, localName, localDate);
	}
	private void saveLocalHighscore() {
		DatabaseInterface DI = new DatabaseInterface(context);
		DI.addLocalHighscore(highscoreObject);
		DI.close();

	}
	private void saveGlobalHighscore() {
		UUID_Manager uuid_m = new UUID_Manager(context);
		
		GlobalHighscoreSender ghss = new GlobalHighscoreSender();
		ghss.setFormData(uuid_m.getUUID(), countryString, highscoreObject);
		ghss.sendFormData();
	}
	
	public int getInt(int value){
		return value;
	}

	@Override
	public void onLocationChanged(Location location) {
		try {
			List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
			for(Address address : addresses) {
				countryString = address.getCountryName();
			}
		} catch (Exception e) {
			Log.e("Locator", "Could not get location data: " + e.toString());
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onResume() {
		super.onResume();
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100000, 100000, this);
		
	}
	
	@Override
	public void onStop() {
		super.onStop();
		locationManager.removeUpdates(this);
		
	}

	private void resetPlayerData() {
		DatabaseInterface DI = new DatabaseInterface(context);
		DI.clearHistory();				
		PlayerObject playerObject = new PlayerObject(context);
		playerObject.Reset();	
		playerObject.close();
	}
	
}

