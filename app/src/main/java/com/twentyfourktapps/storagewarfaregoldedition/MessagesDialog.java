package com.twentyfourktapps.storagewarfaregoldedition;
import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class MessagesDialog extends Dialog {

	Context context;
	ArrayList<MessageStruct> curMessages;
	
	public MessagesDialog(Context pContext, int theme) {
		super(pContext, theme);
		context = pContext;
		
		DatabaseInterface DI = new DatabaseInterface(context);
		curMessages = DI.getMessages();
	}

	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.messagesdialog_layout);
		
		createMessageTable();
	}
	
	public void setScreenAnchorPoint(int width, int height) {
		int anchX = width / 8;
		int anchY = (int)((float)height / 9.5f);
		LinearLayout screenLayout = (LinearLayout)findViewById(R.id.messagesdialog_layout_root);
		screenLayout.setPadding(anchX, anchY, anchX, (anchY * 2));
	}
	
	private void createMessageTable() {
		final ScrollView sv = (ScrollView)findViewById(R.id.messagedialog_scrollview);
		LinearLayout root = new LinearLayout(context);
		root.setOrientation(LinearLayout.VERTICAL);
		root.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.MATCH_PARENT, 1));
		sv.addView(root);
		
		Bitmap jenFace = BitmapFactory.decodeResource(context.getResources(), R.drawable.jenny_yelling);
		Bitmap marcFace = BitmapFactory.decodeResource(context.getResources(), R.drawable.marc_yelling);
		Bitmap terryFace = BitmapFactory.decodeResource(context.getResources(), R.drawable.terry_yelling);
		
		if(!curMessages.isEmpty()) {
			for(int i = 0; i < curMessages.size(); i++) {
				LinearLayout row = new LinearLayout(context);
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT, 1);
				
				ImageView portraitPic = new ImageView(context);
				if(curMessages.get(i).getCharacter().equals("jen")) portraitPic.setImageBitmap(jenFace);
				else if(curMessages.get(i).getCharacter().equals("marc")) portraitPic.setImageBitmap(marcFace);
				else if(curMessages.get(i).getCharacter().equals("terry")) portraitPic.setImageBitmap(terryFace);
				else portraitPic.setImageBitmap(jenFace);
				
				TextView message = new TextView(context);
				message.setGravity(Gravity.CENTER_VERTICAL);
				message.setPadding(10, 0, 10, 0);
				message.setTextAppearance(context, R.style.bankdialog_statement_text);
				message.setText(curMessages.get(i).getMessage());
				
				if(i % 2 == 0) {
					row.addView(portraitPic);
					row.addView(message, params);
				} else {
					row.addView(message, params);
					row.addView(portraitPic);
				}
				
				root.addView(row);
				
				sv.post(new Runnable () {
					public void run() {
						sv.fullScroll(ScrollView.FOCUS_DOWN);				
					}			
				});
			}
		}
	}
}

