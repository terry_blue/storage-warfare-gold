package com.twentyfourktapps.storagewarfaregoldedition;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.twentyfourktapps.storagewarfaregoldedition.LockerUnitManager.HistoryObject;

public class HomeScreen extends Activity {
	
	final private String TAG = "HomeScreen Activity";
	Context context;
	GameSounds gameSounds;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.homescreen_layout);
		context = this;
		
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);		
		gameSounds = new GameSounds(context);
		
		testForNewMessages();
		setButtonListeners();
		testForMainWrapupDialog();
	}
	
	private void testForNewMessages() {
		String[] higherTierLockerNumbers = new String[] { 	"157", "161", "164", "166", "173", "177", "178", "197", "199", "200", "201", "202", "203", "204",
															"310", "311", "312", "313", "314", "325", "326", "327", "328", "329", "343", "344", "345", "346", "347", "348", "349", "350",
															"536", "537", "538", "539", "540", "541", "542", "543", "544", "545", "546", "547", "548", "549", "550", "551", "552", "553", "544" };
		boolean[] isMessageDone = new boolean[11];
		DatabaseInterface DI = new DatabaseInterface(context);
		ArrayList<MessageStruct> curMessages = DI.getMessages();
		DI = new DatabaseInterface(context);
		ArrayList<HistoryObject> history = DI.getHistory();
		DI = new DatabaseInterface(context);
		
		for(int i = 0; i < isMessageDone.length; i++) {
			isMessageDone[i] = false;
		}
		for (int i = 0; i < curMessages.size(); i++) {
			isMessageDone[curMessages.get(i).getMessageID()] = true;
		}
		PlayerObject player = new PlayerObject(context);
		
		final String CHAR_JEN = "jen";
		final String CHAR_MARC = "marc";
		final String CHAR_TERRY = "terry";
		final String CHAR_NICK = "nick";
		
		//Money less than $700 - id 0
		if(player.currentScore < 700 && (!isMessageDone[0])) {
			DI.addMessage(new MessageStruct("Looks like you're running out of money! I would be more careful with the units you are purchasing, or you will be out of this business very soon!", CHAR_JEN, 0));
		}
		
		//Money passes $2000 - id 1
		if(player.currentScore >= 2000 && !(isMessageDone[1])) {
			DI.addMessage(new MessageStruct("Well isn't that nice. You seem to KIND OF know what you are doing. We will see how long it lasts though.", CHAR_JEN, 1));
		}
		
		//Money passes $3500 - id 2
		if(player.currentScore >= 3500 && (!isMessageDone[2])) {
			DI.addMessage(new MessageStruct("You seem to be getting awful lucky out there, I like to call it beginners luck!", CHAR_MARC, 2));
		}
		
		//Money passes $5000 - id 3
		if(player.currentScore >= 5000 && (!isMessageDone[3])) {
			DI.addMessage(new MessageStruct("I may have under estimated you. You're actually doing a good job. You might have a future in this business!", CHAR_TERRY, 3));
		}
		
		//Money passes $10000 - id 4
		if(player.currentScore >= 10000 && (!isMessageDone[4])) {
			DI.addMessage(new MessageStruct("If you keep this up, you might put the rest of us out of business! How about you slow down and let someone else make a little money!", CHAR_TERRY, 4));
		}
		
		//Player attends 5 auctions - id 5
		if(history.size() >= 15 && !(isMessageDone[5])) {
			DI.addMessage(new MessageStruct("How are you holding up? This business isn't too hard for you is it?", CHAR_MARC, 5));
		}
		
		//Player attends 10 auctions - id 6
		if(history.size() >= 30 && !(isMessageDone[6])) {
			DI.addMessage(new MessageStruct("It looks like you are taking this business pretty seriously. I look forward to outbidding you in the years to come!", CHAR_TERRY, 6));
		}
		
		//Player attends Top Shelf Storage - id 7
		for(int i = 0; i < higherTierLockerNumbers.length; i++) {
			for(int a = 0; a < history.size(); a++) {
				if(history.get(a).title.contains(higherTierLockerNumbers[i]) && !(isMessageDone[7])) {
					DI.addMessage(new MessageStruct("So you have decided to come out and play with the big boys(and girls) at Top Shelf Storage? I am impressed!", CHAR_MARC, 7));
					i = higherTierLockerNumbers.length;
					break;
				}
			}
		}
		
		int highestProfit = 0;
		int highestLoss = 0;
		
		for(int i = 0; i < history.size(); i++) {
			int total = Integer.parseInt(history.get(i).in) - Integer.parseInt(history.get(i).out);
			if(total > highestProfit) {
				highestProfit = getInt(total);			
			} else if (total < highestLoss) {
				highestLoss = getInt(total);
			}
		}
		
		
		//Player has a loss of more than $700 - id 9
		if(highestLoss <= -700 && !(isMessageDone[9])) {
			DI.addMessage(new MessageStruct("You might as well head to the beach, you aren't making any money doing this!", CHAR_NICK, 9));
			Log.e(TAG, "highestLoss = " + Integer.toString(highestLoss));
			
		}
		
		//Player makes a profit of more than $2000 - id 10
		if(highestProfit >= 20 && !(isMessageDone[10])) {
			DI.addMessage(new MessageStruct("So you think you can come to Jersey and win all our auctions? Next time I won't go so easy on you!", CHAR_NICK, 10));
			Log.e(TAG, "highestProfit = " + Integer.toString(highestProfit));
		}
		
		DI.close();
		
	}
	
	private void setButtonListeners() {
		final Button bankButton = (Button)findViewById(R.id.homescreen_button_bank);
		final Button itemsButton = (Button)findViewById(R.id.homescreen_button_items);
		final Button scheduleButton = (Button)findViewById(R.id.homescreen_button_schedule);
		final Button messagesButton = (Button)findViewById(R.id.homescreen_button_messages);
		final LinearLayout cpuScreenLayout = (LinearLayout)findViewById(R.id.homescreen_linearlayout_root);
		
		bankButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				BankDialog bankDialog = new BankDialog(context, R.style.laptop_white_screen);
				bankDialog.show();
				bankDialog.setScreenAnchorPoint(cpuScreenLayout.getWidth(), cpuScreenLayout.getHeight());
				gameSounds.playClick();
			}
		});
		
		itemsButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				gameSounds.playClick();
				ItemsCollectionDialog itemsCollectionDialog = new ItemsCollectionDialog(context, R.style.laptop_white_screen);
				itemsCollectionDialog.show();
				itemsCollectionDialog.setScreenAnchorPoint(cpuScreenLayout.getWidth(), cpuScreenLayout.getHeight());
				
			}
		});
		
		scheduleButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				PlayerObject player = new PlayerObject(context);
				//Log.e("Home Screen", "Months played = " + Integer.toString(player.monthsPlayed) + " daysPlayed = " + Integer.toString(player.daysPlayed));
				if(player.monthsPlayed >= 12) {
					startActivity(new Intent(context, GameOverActivity.class));
				} else {
					ScheduleDialog scheduleDialog = new ScheduleDialog(context, R.style.laptop_white_screen);				
					scheduleDialog.show();
					scheduleDialog.setScreenAnchorPoint(cpuScreenLayout.getWidth(), cpuScreenLayout.getHeight());
				}
				gameSounds.playClick();
				
			}
		});
		
		messagesButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				MessagesDialog msgDialog = new MessagesDialog(context, R.style.laptop_white_screen);
				msgDialog.show();
				msgDialog.setScreenAnchorPoint(cpuScreenLayout.getWidth(), cpuScreenLayout.getHeight());
				gameSounds.playClick();
				
			}
		});
		
		//Set the typeface (fonts) of the buttons
		Typeface armWrestlerTTF = Typeface.createFromAsset(context.getAssets(), "fonts/ArmWrestler.ttf");
		Typeface CandalTTF = Typeface.createFromAsset(context.getAssets(), "fonts/Candal.ttf");
		Typeface mishmashTTF = Typeface.createFromAsset(context.getAssets(), "fonts/mishmash.ttf");
		Typeface ultraTTf = Typeface.createFromAsset(context.getAssets(), "fonts/Ultra.ttf");
		
		bankButton.setTypeface(mishmashTTF);
		scheduleButton.setTypeface(armWrestlerTTF);
		itemsButton.setTypeface(CandalTTF);
		messagesButton.setTypeface(ultraTTf);
	}

	private void testForMainWrapupDialog() {
		//final LinearLayout cpuScreenLayout = (LinearLayout)findViewById(R.id.homescreen_linearlayout_root);
		DatabaseInterface DI = new DatabaseInterface(context);
		ArrayList<HistoryObject> history = DI.getHistory();
		
		if(history.size() > 2) {
			MainWrapupDialog dialog = new MainWrapupDialog(context, R.style.laptop_white_screen);
			//dialog.setScreenAnchorPoint(cpuScreenLayout.getWidth(), cpuScreenLayout.getHeight());
			dialog.show();
		}
	}
	
	static public int getInt(int value) {
		return value;
	}
}

