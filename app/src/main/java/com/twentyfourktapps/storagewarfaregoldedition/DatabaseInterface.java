package com.twentyfourktapps.storagewarfaregoldedition;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.twentyfourktapps.storagewarfaregoldedition.LockerUnitManager.HistoryObject;

//if possible, limit the use of this object to one instance per activity - memory is tight!
public class DatabaseInterface {

	DatabaseClass theDB;
	
	//Constructor
	public DatabaseInterface(Context pContext) {
		theDB = new DatabaseClass(pContext, "Storage_Wars_Jersey_Beach_DB", null, 1);
	}
	
	public void close() {
		theDB.close();
	}
	
	//Player Interface Methods
	public void updatePlayersRow(String pName, int pScore, boolean pIsActive, String monthsView, String daysView) {
		theDB.updatePlayersRow(pName, pScore, pIsActive, monthsView, daysView);
	}
	public PlayerStruct getPlayerStruct() {
		return theDB.getPlayerFields();
	}
	
	//Message Interface Methods
	public void addMessage(MessageStruct msg) {
		theDB.addMessage(msg);
	}
	public ArrayList<MessageStruct> getMessages() {
		return theDB.getMessages();
	}
	public void clearMessages() {
		theDB.clearMessages();
	}
	
	//History Interface Methods
	public ArrayList<HistoryObject> getHistory() {
		 ArrayList<HistoryObject> toReturn = theDB.getHistory();
		 theDB.close();
		 return toReturn;
	}
	public void addHistoryObject(HistoryObject HO) {
		theDB.addHistoryObject(HO);
		theDB.close();
	}
	public void clearHistory() {
		theDB.clearHistory();
		theDB.close();
	}
	
	//Local Highscore Interface Methods
	public void addLocalHighscore(HighscoreObject HO) {
		theDB.addHighscore(HO);
		theDB.close();
	}
	public ArrayList<HighscoreObject> getLocalHighscores() {
		ArrayList<HighscoreObject> highscores = theDB.getLocalHighscores();
		theDB.close();		
		return highscores;
	}
	
	
	
	private class DatabaseClass extends SQLiteOpenHelper {
		
		protected SQLiteDatabase DB;
		
		//Fields for PlayerObject
		protected final String PLAYER_TABLE_NAME = "player_table";
		protected final String PLAYER_TABLE_ROW_ID = "id";
		protected final String PLAYER_TABLE_ROW_NAME = "name";
		protected final String PLAYER_TABLE_ROW_SCORE = "score";
		protected final String PLAYER_TABLE_ROW_ISACTIVE = "is_active";
		protected final String PLAYER_TABLE_ROW_MONTHS_VIEWED = "months_viewed";
		protected final String PLAYER_TABLE_ROW_DAYS_VIEWED = "days_viewed";
		
		//Fields for Locker History
		protected final String HISTORY_TABLE_NAME = "locker_history_table";
		protected final String HISTORY_ROW_ID = "id";
		protected final String HISTORY_ROW_DATE = "date";
		protected final String HISTORY_ROW_TITLE = "title";
		protected final String HISTORY_ROW_OUT = "money_out";
		protected final String HISTORY_ROW_IN = "money_in";
				
		//Fields for Messages Table
		protected final String MESSAGES_TABLE_NAME = "messages_table";
		protected final String MESSAGES_ROW_ID = "row_id";
		protected final String MESSAGES_ROW_MESSAGEID = "message_id";
		protected final String MESSAGES_ROW_CHARACTER = "character";
		protected final String MESSAGES_ROW_MESSAGE = "message";
		
		//Fields for Local High Score Objects
		protected final String LOCAL_HIGHSCORE_TABLE_NAME = "local_highsore_table";
		protected final String LOCAL_HIGHSCORE_ROW_ID = "id";
		protected final String LOCAL_HIGHSCORE_ROW_NAME = "name";
		protected final String LOCAL_HIGHSCORE_ROW_DATE = "date";
		protected final String LOCAL_HIGHSCORE_ROW_TOTALIN = "total_in";
		protected final String LOCAL_HIGHSCORE_ROW_TOTALOUT = "total_out";
		protected final String LOCAL_HIGHSCORE_ROW_END_SCORE = "end_score";
		protected final String LOCAL_HIGHSCORE_ROW_BIGGEST_WIN = "biggest_win";
		protected final String LOCAL_HIGHSCORE_ROW_BIGGEST_LOSS = "biggest_loss";
		protected final String LOCAL_HIGHSCORE_ROW_WIN_STREAK = "win_streak";
		protected final String LOCAL_HIGHSCORE_ROW_LOSS_STREAK = "loss_streak";	
		
		
		//Constructor
		public DatabaseClass(Context context, String name, CursorFactory factory, int version) {
			super(context, name, factory, version);
				DB = getWritableDatabase();
		}		

		@Override
		public void onCreate(SQLiteDatabase arg0) {
			DB = arg0;
			
			//**Create Player Table**\\
			String newTableString = "CREATE TABLE " + PLAYER_TABLE_NAME +
					" (" + PLAYER_TABLE_ROW_ID + " integer primary key not null," +
					PLAYER_TABLE_ROW_NAME + " text," +
					PLAYER_TABLE_ROW_SCORE + " text, " + 
					PLAYER_TABLE_ROW_ISACTIVE + " text, " +
					//PLAYER_TABLE_ROW_HISTORY + " text, " +
					PLAYER_TABLE_ROW_MONTHS_VIEWED + " text, " +
					PLAYER_TABLE_ROW_DAYS_VIEWED + " text" +
					");";
			DB.execSQL(newTableString);
			//add a players row to the database
			ContentValues values = new ContentValues();
			values.put(PLAYER_TABLE_ROW_ID, 1);
			values.put(PLAYER_TABLE_ROW_NAME, "Spencer");
			values.put(PLAYER_TABLE_ROW_SCORE, "0");
			values.put(PLAYER_TABLE_ROW_ISACTIVE, "false");	
			values.put(PLAYER_TABLE_ROW_MONTHS_VIEWED, "0");
			values.put(PLAYER_TABLE_ROW_DAYS_VIEWED, "0");
			try {
				DB.insert(PLAYER_TABLE_NAME, null, values);
			} catch (Exception e) {
				Log.e("PlayerObject", e.toString());
				
			}
			
			
			//**Create Local High Score Table **\\
			newTableString = "CREATE TABLE " + LOCAL_HIGHSCORE_TABLE_NAME +
					" (" + LOCAL_HIGHSCORE_ROW_ID + " integer primary key not null," +
					LOCAL_HIGHSCORE_ROW_NAME + " text, " +
					LOCAL_HIGHSCORE_ROW_DATE + " text, " + 
					LOCAL_HIGHSCORE_ROW_TOTALIN + " text, " +
					LOCAL_HIGHSCORE_ROW_TOTALOUT + " text, " +
					LOCAL_HIGHSCORE_ROW_END_SCORE + " text, " +
					LOCAL_HIGHSCORE_ROW_BIGGEST_WIN + " text, " +
					LOCAL_HIGHSCORE_ROW_BIGGEST_LOSS + " text, " +
					LOCAL_HIGHSCORE_ROW_WIN_STREAK + " text, " +
					LOCAL_HIGHSCORE_ROW_LOSS_STREAK + " text" +
					");";
			DB.execSQL(newTableString);
			
			//**Create Messages Table**\\
			newTableString ="CREATE TABLE " + MESSAGES_TABLE_NAME +
					" (" + MESSAGES_ROW_ID + " integer primary key autoincrement not null, " +
					MESSAGES_ROW_MESSAGEID + " integer, " +
					MESSAGES_ROW_CHARACTER + " text, " +
					MESSAGES_ROW_MESSAGE + " text" +
					");";
			DB.execSQL(newTableString);
			
			//**Create History Table**\\
			newTableString = "CREATE TABLE " + HISTORY_TABLE_NAME +
					" (" + HISTORY_ROW_ID + " integer primary key autoincrement not null, " +
					HISTORY_ROW_DATE + " text, " +
					HISTORY_ROW_TITLE + " text," +
					HISTORY_ROW_IN + " text, " + 
					HISTORY_ROW_OUT + " text " +
					");";
			DB.execSQL(newTableString);
			
			//DB.close();
		}

		@Override
		public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
			// TODO Auto-generated method stub
			
		}
		
		/////*****METHODS FOR PLAYER DB INTERFACE*****/////
		public void updatePlayersRow(String playerName, int playerCash, boolean activeness, String viewedDays, String dayTier) {
			ContentValues values = new ContentValues();
			values.put(PLAYER_TABLE_ROW_NAME, playerName);
			values.put(PLAYER_TABLE_ROW_SCORE, Integer.toString(playerCash));
			values.put(PLAYER_TABLE_ROW_ISACTIVE, Boolean.toString(activeness));
			values.put(PLAYER_TABLE_ROW_MONTHS_VIEWED, viewedDays);
			values.put(PLAYER_TABLE_ROW_DAYS_VIEWED, dayTier);
			DB.update(PLAYER_TABLE_NAME, values, PLAYER_TABLE_ROW_ID + " = 1", null);
		}
		public PlayerStruct getPlayerFields() {
			PlayerStruct struct = new PlayerStruct();
			
			Cursor cursor;
			
			try {
				cursor = DB.query(PLAYER_TABLE_NAME,
						new String[] { PLAYER_TABLE_ROW_NAME, PLAYER_TABLE_ROW_SCORE, PLAYER_TABLE_ROW_ISACTIVE, PLAYER_TABLE_ROW_MONTHS_VIEWED, PLAYER_TABLE_ROW_DAYS_VIEWED },
						PLAYER_TABLE_ROW_ID + " = 1",
						null, null, null, null);
				cursor.moveToFirst();
				if(!cursor.isAfterLast()) {
					struct.name = cursor.getString(0);
					struct.cash = Integer.parseInt(cursor.getString(1));
					struct.isActive = (cursor.getString(2).equals("true")) ? true : false;
					struct.viewedMonths = cursor.getInt(3);
					struct.viewedTiers = cursor.getInt(4);
				}
				cursor.close();
			} catch (Exception e) {
				Log.e("DatabaseInterface", "getPlayerFields() " + e.toString());
			}
			return struct;
		}
		
		/////*****METHODS FOR MESSAGES*****/////
		public void addMessage(MessageStruct messageStruct) {
			ContentValues values = new ContentValues();
			values.put(MESSAGES_ROW_CHARACTER, messageStruct.getCharacter());
			values.put(MESSAGES_ROW_MESSAGE, messageStruct.getMessage());
			values.put(MESSAGES_ROW_MESSAGEID, messageStruct.getMessageID());
			Log.e("DI MESSAGE ROW ID", Integer.toString(messageStruct.getMessageID()));
			try {
				DB.insert(MESSAGES_TABLE_NAME, null, values);
			} catch (Exception e) {
				Log.e("DI ADD MESSAGE ERROR", e.toString());
			}
		}
		public ArrayList<MessageStruct> getMessages() {
			ArrayList<MessageStruct> messages = new ArrayList<MessageStruct>();
			Cursor cursor;
			
			try {
				cursor = DB.query(MESSAGES_TABLE_NAME, new String[] { MESSAGES_ROW_MESSAGEID, MESSAGES_ROW_CHARACTER, MESSAGES_ROW_MESSAGE }, null, null, null, null, null);
				cursor.moveToFirst();
				while(!cursor.isAfterLast()) {
					MessageStruct msg = new MessageStruct(cursor.getString(2), cursor.getString(1), cursor.getInt(0));
					messages.add(msg);
					cursor.moveToNext();
				}
				cursor.close();
			} catch (Exception e) {
				Log.e("DI getMessages() error", e.toString());
			}
			close();
			return messages;
		}
		public void clearMessages() {
			DB.delete(MESSAGES_TABLE_NAME, null, null);
			close();
		}
		
		/////*****METHODS FOR LOCAL HIGH SCORE DB*****/////
		public void addHighscore(HighscoreObject theHighscore){
			ContentValues values = new ContentValues();
			values.put(LOCAL_HIGHSCORE_ROW_NAME, theHighscore.name);
			values.put(LOCAL_HIGHSCORE_ROW_DATE, Long.toString(theHighscore.date));
			values.put(LOCAL_HIGHSCORE_ROW_TOTALIN, theHighscore.totalIn);
			values.put(LOCAL_HIGHSCORE_ROW_TOTALOUT, theHighscore.totalOut);
			values.put(LOCAL_HIGHSCORE_ROW_END_SCORE, theHighscore.endScore);
			values.put(LOCAL_HIGHSCORE_ROW_BIGGEST_WIN, theHighscore.biggestWin);
			values.put(LOCAL_HIGHSCORE_ROW_BIGGEST_LOSS, theHighscore.biggestLoss);
			values.put(LOCAL_HIGHSCORE_ROW_WIN_STREAK, theHighscore.winStreak);
			values.put(LOCAL_HIGHSCORE_ROW_LOSS_STREAK, theHighscore.lossStreak);
			
			try {
				DB.insert(LOCAL_HIGHSCORE_TABLE_NAME, null, values);
				DB.close();
			} catch (Exception e) {
				Log.e("Database Interface", "error adding local highscore: " + e.toString());
			}
		}
		public ArrayList<HighscoreObject> getLocalHighscores() {
			ArrayList<HighscoreObject> allLocalHighscores = new ArrayList<HighscoreObject>();
			Cursor cursor;
			
			try {
				cursor = DB.query(LOCAL_HIGHSCORE_TABLE_NAME, new String[] { LOCAL_HIGHSCORE_ROW_ID, LOCAL_HIGHSCORE_ROW_NAME, LOCAL_HIGHSCORE_ROW_DATE, LOCAL_HIGHSCORE_ROW_TOTALIN, LOCAL_HIGHSCORE_ROW_TOTALOUT, LOCAL_HIGHSCORE_ROW_BIGGEST_WIN, LOCAL_HIGHSCORE_ROW_BIGGEST_LOSS, LOCAL_HIGHSCORE_ROW_WIN_STREAK, LOCAL_HIGHSCORE_ROW_LOSS_STREAK}, null, null, null, null, LOCAL_HIGHSCORE_ROW_END_SCORE + " DESC");
				cursor.moveToFirst();
				while(!cursor.isAfterLast()) {
					HighscoreObject HO = new HighscoreObject();
					int localID = cursor.getInt(0);
					String localName = cursor.getString(1);
					long localDate = cursor.getLong(2);
					int totalIn = cursor.getInt(3);
					int totalOut = cursor.getInt(4);
					int biggestWin = cursor.getInt(5);
					int biggestLoss = cursor.getInt(6);
					int winStreak = cursor.getInt(7);
					int lossStreak = cursor.getInt(8);
					HO.setData(0, 0, localID, totalIn, totalOut, biggestLoss, biggestWin, winStreak, lossStreak, localName, localDate);
					
					allLocalHighscores.add(HO);
					cursor.moveToNext();
				}
				cursor.close();
				
			} catch (Exception e) {
				Log.e("Database Interface", "Error getting all local highscores: " + e.toString());
			}
			
			return allLocalHighscores;
		}
		
		/////*****METHODS FOR LOCKER HISTORY*****/////
		public ArrayList<HistoryObject> getHistory() {
			ArrayList<HistoryObject> historyUnits = new ArrayList<HistoryObject>();
			
			Cursor cursor;
			
			try {
				cursor = DB.query(HISTORY_TABLE_NAME, new String[] {HISTORY_ROW_ID, HISTORY_ROW_DATE, HISTORY_ROW_TITLE, HISTORY_ROW_IN, HISTORY_ROW_OUT}, null, null, null, null, null);
				cursor.moveToFirst();
				if (!cursor.isAfterLast()) {
					do {
						HistoryObject hO =  new HistoryObject(cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
						historyUnits.add(hO);
					} while (cursor.moveToNext());
				}
				cursor.close();
			} catch (Exception e) {
				Log.e("Database Interface", "getHistory() error: " + e.toString());
			}
			DB.close();
			return historyUnits;
		}
		public void addHistoryObject(HistoryObject historyObject) {
			ContentValues values = new ContentValues();
			values.put(HISTORY_ROW_DATE, historyObject.date);
			values.put(HISTORY_ROW_TITLE, historyObject.title);
			values.put(HISTORY_ROW_IN, historyObject.in);
			values.put(HISTORY_ROW_OUT, historyObject.out);
			try {
				DB.insert(HISTORY_TABLE_NAME, null, values);
			} catch (Exception e) {
				Log.e("Database Interface", "error adding history object to table: " + e.toString());
			}
		}
		public void clearHistory() {
			DB.delete(HISTORY_TABLE_NAME, null, null);			
		}
	
	}
}
