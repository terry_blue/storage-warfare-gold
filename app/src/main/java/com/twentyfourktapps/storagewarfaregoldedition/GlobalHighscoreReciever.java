package com.twentyfourktapps.storagewarfaregoldedition;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class GlobalHighscoreReciever {

	Context context;
	
	HttpClient httpClient;
	HttpPost httpPost;
	ArrayList<NameValuePair> nameValuePairs;
	InputStream inputStream = null;
	String tempResult = "";
	String result = "";
	final String GET_URL = "http://drunkzombies.com/Android/Highscores/Games/get_storagewarfare_complete.php";
	
	ArrayList<HighscoreObject> globalHighscoreObjectArray;	
	String usersRank;
	String usersHighestScore;


	
	public GlobalHighscoreReciever(Context pContext, String uuid, String boardType, String region) {
		context = pContext;
		nameValuePairs = new ArrayList<NameValuePair>();
		globalHighscoreObjectArray = new ArrayList<HighscoreObject>();
		
		//Users score/rank http request
		try {
			httpClient = new DefaultHttpClient();
			httpPost = new HttpPost(GET_URL);
			nameValuePairs.add(new BasicNameValuePair("uuid", uuid));
			nameValuePairs.add(new BasicNameValuePair("boardtype", boardType));
			nameValuePairs.add(new BasicNameValuePair("dataneeded", "personal"));
			nameValuePairs.add(new BasicNameValuePair("region", region));
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			inputStream = entity.getContent();
		} catch (Exception e) {
			Log.e("GLOBAL HIGHSCORE RECIEVER 1", e.toString());
		}
		
		//Convert the score/rank request result to a string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 32);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line +"\n");
				Log.i("RESULT", line);
			}
			inputStream.close();
			result = sb.toString();
			
			//parse the temp result
			while(!result.startsWith("[")) {
				result = result.substring(1);
			}
		} catch (Exception e) {
			Log.e("GLOBAL HIGHSCORE RECIEVER 2", e.toString());
		}
		
		
		//parse the users data result
		try {
			result = result.substring(1, result.length() - 2);
			String [] tokens = result.split("@");
			usersRank = tokens[0];
			usersHighestScore = tokens[1];
		} catch (Exception e) {
			Log.e("GLOBAL HIGHSCORE RECIEVER", e.toString());
		}
		
		//Highscore array http request
		try {
			httpClient = new DefaultHttpClient();
			httpPost = new HttpPost(GET_URL);
			nameValuePairs.add(new BasicNameValuePair("uuid", uuid));
			nameValuePairs.add(new BasicNameValuePair("boardtype", boardType));
			nameValuePairs.add(new BasicNameValuePair("dataneeded", "scores"));
			nameValuePairs.add(new BasicNameValuePair("region", region));
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			inputStream = entity.getContent();
			
		} catch(Exception e) {
			Toast toast = Toast.makeText(pContext, "Connection error, check internet connection" , Toast.LENGTH_LONG);
			toast.show();
			Log.e("GLOBAL HIGHSCORE RECIEVER 3", e.toString());
		}
		  
		
		//convert the http highscore response to a string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 32);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line +"\n");
				Log.i("RESULT", line);
			}
			inputStream.close();
			result = sb.toString();
			
			//parse the temp result
			while(!result.startsWith("[")) {
				result = result.substring(1);
			}
		} catch (Exception e) {
			Log.e("GLOBAL HIGHSCORE RECIEVER 4", "Error converting result "+e.toString());
		}
		
		
		//parse JSON data
		try {
			JSONArray jArray = new JSONArray(result);
			for(int i = 0; i < jArray.length(); i++) {
				JSONObject json_data = jArray.getJSONObject(i);
				//put the data into a GlobalHighscoreObject
				int recordId = json_data.getInt("RecordID");
				//int userId = json_data.getInt("UserID");
				String name = json_data.getString("Name");
				int cashIn = json_data.getInt("TotalIn");
				int cashOut = json_data.getInt("TotalOut");
				//int score = json_data.getInt("EndScore");
				//long date = json_data.getLong("Date");
				//int biggestWin = json_data.getInt("BiggestWin");
				//int biggestLoss = json_data.getInt("BiggestLoss");
				//int winStreak = json_data.getInt("WinStreak");
				//int lossStreak = json_data.getInt("LossStreak");
				HighscoreObject hsO = new HighscoreObject();
				//hsO.setData(recordId, userId, recordId, cashIn, cashOut, biggestLoss, biggestWin, winStreak, lossStreak, name, date);
				hsO.setData(recordId, 0, recordId, cashIn, cashOut, 0, 0, 0, 0, name, 0);
				//add the data to the GlobalHighscoreObjectArray
				globalHighscoreObjectArray.add(hsO);
			}
		} catch (Exception e) {
			Log.e("GLOBAL HIGHSCORE RECIEVER 5", "Error parsing JSON data "+e.toString());
		}
	}
	
	
	
	public ArrayList<HighscoreObject> getHighscoreObjects() {
		return globalHighscoreObjectArray;
	}
	
	String getUsersRank() {
		if (usersRank != null) return usersRank;		
		return "Unknown";
	}
	String getUsersHighestScore() {
		if(usersHighestScore != null) return usersHighestScore;
		return "Unknown";
	}
}

