package com.twentyfourktapps.storagewarfaregoldedition;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;

public class ServerUtil {

	final static String TAG = "ServerUtil";
	final private static boolean showResponse = true;
	
	final public static String WEB_PREFIX = "http://www.blueshroom.co.uk/sureants/ants.php";
	
	public static String getResponse(String targetAddress) {		
		return getResponse(targetAddress, new ArrayList<NameValuePair>());
	}
	
	
	public static String getResponse(String targetAddress, ArrayList<NameValuePair> postPairs) {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(targetAddress);
			httpPost.setEntity(new UrlEncodedFormEntity(postPairs));
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity entity = response.getEntity();			
			final InputStream inputStream = entity.getContent();
			
			//Read the response
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 32);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while((line = reader.readLine()) != null) {
					sb.append(line);					
				}
				if(showResponse) Log.i(TAG, "response for targetAddress " + targetAddress + " = " + sb.toString());
				return sb.toString();
				
			} catch(Exception e) {
				Log.e(TAG, "error reading response for targetAddress: " + targetAddress + "... " + e.toString());
			}
		} catch (UnknownHostException e) {
			Log.e(TAG, "UnknownHostException: " + e.toString());
			return "host_error";
		} catch(Exception e) {
			Log.e(TAG, "getResponse() error for address: " + targetAddress + "... " + e.toString());
		}
		return "fail";
	}
}
