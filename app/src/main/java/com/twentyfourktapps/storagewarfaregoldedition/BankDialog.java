package com.twentyfourktapps.storagewarfaregoldedition;
import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.twentyfourktapps.storagewarfaregoldedition.LockerUnitManager.HistoryObject;



public class BankDialog extends Dialog{
	
	Context context;
	
	public BankDialog(Context pContext, int theme) {
		super(pContext, theme);
		context = pContext;
		
	}

	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bankdialog_layout);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		PlayerObject player = new PlayerObject(context);
		
		//set views
		TextView accountNameTV = (TextView)findViewById(R.id.bankdialog_textview_account_name);
		TextView balanceTV = (TextView)findViewById(R.id.bankdialog_textview_balance);
		
		accountNameTV.setText(player.name);
		balanceTV.setText(Integer.toString(player.currentScore));
		
		createStatementTable();
		
		//Scroll the scroll viewer to the bottom
		final ScrollView sv = (ScrollView)findViewById(R.id.bankdialog_scrollview_statement);
		sv.post(new Runnable () {
			public void run() {
				sv.fullScroll(ScrollView.FOCUS_DOWN);				
			}			
		});
		
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		dismiss();
	}

	public void createStatementTable() {
		TableLayout table = (TableLayout)findViewById(R.id.bankdialog_tablelayout_statement);
		table.removeAllViews();
		final TableRow.LayoutParams tableRowLayoutParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1);
		DatabaseInterface DI = new DatabaseInterface(context);		
		ArrayList<HistoryObject> history = DI.getHistory();
		DI.close();
		
		TableRow row = new TableRow(context);
		row.setLayoutParams(new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1));
		
		
		for (int i = 0; i < history.size(); i++) {
			if(!(history.get(i).profit.equals("0"))) {
			TableRow tableRow = new TableRow(context);
			
			//Date 
			String dateString = history.get(i).date;
			TextView dateTV = new TextView(context);			
			dateTV.setWidth(0);
			dateTV.setText(dateString);
			dateTV.setTextAppearance(context, R.style.bankdialog_statement_text);
			tableRow.addView(dateTV, tableRowLayoutParams);
			
			//Title
			String titleString = history.get(i).title;
			TextView titleTV = new TextView(context);			
			titleTV.setWidth(0);
			titleTV.setText(titleString);
			titleTV.setTextAppearance(context, R.style.bankdialog_statement_text);
			tableRow.addView(titleTV, tableRowLayoutParams);
			
			
			//In
			String inString = history.get(i).in;
			TextView inTV = new TextView(context);			
			inTV.setWidth(0);
			inTV.setText(inString);
			inTV.setGravity(Gravity.RIGHT);
			inTV.setTextAppearance(context, R.style.bankdialog_statement_text);
			tableRow.addView(inTV, tableRowLayoutParams);
						
			//Out
			String outString = history.get(i).out;
			TextView outTV = new TextView(context);			
			outTV.setWidth(0);
			outTV.setText(outString);
			outTV.setGravity(Gravity.RIGHT);
			outTV.setTextAppearance(context, R.style.bankdialog_statement_text);
			tableRow.addView(outTV, tableRowLayoutParams);
			
			//Profit			
			String profitString = history.get(i).profit;
			TextView profitTV = new TextView(context);			
			profitTV.setWidth(0);
			profitTV.setText(profitString);
			profitTV.setGravity(Gravity.CENTER);
			profitTV.setTextAppearance(context, R.style.bankdialog_statement_text);
			tableRow.addView(profitTV, tableRowLayoutParams);
			
			//tableRow.addView(dateTV, tableRowLayoutParams);
			
			
			table.addView(tableRow);
			}
		}
	}
	public void setScreenAnchorPoint(int width, int height) {
		int anchX = width / 8;
		int anchY = (int)((float)height / 9.5f);
		LinearLayout screenLayout = (LinearLayout)findViewById(R.id.bankdialog_layout_root);
		screenLayout.setPadding(anchX, anchY, anchX, (anchY * 2));
	}
}
