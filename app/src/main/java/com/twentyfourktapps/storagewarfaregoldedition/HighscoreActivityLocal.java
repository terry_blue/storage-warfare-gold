package com.twentyfourktapps.storagewarfaregoldedition;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup.LayoutParams;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class HighscoreActivityLocal extends HighscoreAbstract{

	DatabaseInterface DI;
	ArrayList<HighscoreObject> highscoreArrayList;
	
	
	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.highscore_activity_local_layout);
		
		DI = new DatabaseInterface(context);
		highscoreArrayList = DI.getLocalHighscores();
		
		populateTable();
	}
	
	private void populateTable() {
		tableLayout = (TableLayout)findViewById(R.id.highscore_tablelayout_local);
		
		for(int i = 0; i < highscoreArrayList.size(); i++) {
			TableRow tr = new TableRow(context);
			TableRow.LayoutParams rowParams = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT, 1);
			
			//Position
			TextView posTV = new TextView(context);
			posTV.setText(Integer.toString(i + 1));
			posTV.setMinimumWidth(40);
			
			//Name
			TextView nameTV = new TextView(context);
			nameTV.setText(highscoreArrayList.get(i).name);
			
			//Total In
			TextView inTV = new TextView(context);
			inTV.setText("$" + (Integer.toString(highscoreArrayList.get(i).totalIn)));
			
			//Total Out
			TextView outTV = new TextView(context);
			outTV.setText("$" + (Integer.toString(highscoreArrayList.get(i).totalOut)));
			
			//End Score
			TextView endScoreTV = new TextView(context);
			endScoreTV.setText("$" + (Integer.toString(highscoreArrayList.get(i).endScore)));
			
			
			tr.addView(posTV); tr.addView(nameTV, rowParams); tr.addView(inTV, rowParams); tr.addView(outTV, rowParams); tr.addView(endScoreTV, rowParams);
			tableLayout.addView(tr);
		}
	}
	
	@Override
	public void onBackPressed() {
		Intent mIntent = new Intent(context, TitleScreen.class);
		mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(mIntent);	
	}
}
