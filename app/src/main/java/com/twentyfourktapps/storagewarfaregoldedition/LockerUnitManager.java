package com.twentyfourktapps.storagewarfaregoldedition;
import java.util.ArrayList;
import java.util.Random;

import android.content.Context;


public class LockerUnitManager {

	ArrayList<LockerUnit> allLockerUnits;
	
	public LockerUnitManager() {
		allLockerUnits = allLockerUnits();
	}
	
	
	
	public LockerUnit getNewUnit(int tier, Context context) {
		ArrayList<String> items = new ArrayList<String>();
		ArrayList<Integer> prices = new ArrayList<Integer>();		
		LockerUnit selectedUnit = new LockerUnit(1, R.drawable.storage_unit, 200, items, prices, "title", LockerUnit.TIER_LOW);
		ArrayList<LockerUnit> lockerUnitChoice = new ArrayList<LockerUnit>();
					
		DatabaseInterface DI = new DatabaseInterface(context);
		ArrayList<HistoryObject> history = DI.getHistory();
		DI.close();
		
		for(int i = 0; i < allLockerUnits.size(); i++) {			
			if(allLockerUnits.get(i).lockerTier == tier) {
				lockerUnitChoice.add(allLockerUnits.get(i));
			}				
		}
		
		for(int i = 0; i < lockerUnitChoice.size(); i++) {
			for(int a = 0; a < history.size(); a++) {
				if(lockerUnitChoice.get(i).lockerTitle.equals(history.get(a).title)) {
					lockerUnitChoice.remove(i);		
					break;
				}
			}
		}
		
		
		
		if(lockerUnitChoice.isEmpty()) {
			for(int i = 0; i < allLockerUnits.size(); i++) {			
				if(allLockerUnits.get(i).lockerTier == tier) {
					lockerUnitChoice.add(allLockerUnits.get(i));					
				}				
			}
		}
		
		
		
		//Pick a random locker unit from the ArrayList of Locker Units
		Random randNum = new Random(System.currentTimeMillis());
		int lockerChoice = randNum.nextInt(lockerUnitChoice.size());
		selectedUnit = lockerUnitChoice.get(lockerChoice);
		
		return selectedUnit;		
	}
	public LockerUnit getLockerNumber(int lockerID){
		for(int i = 0; i < allLockerUnits.size(); i++) {
			if(allLockerUnits.get(i).unitID == lockerID) return allLockerUnits.get(i);
		}		
		return null;
	}
	private ArrayList<LockerUnit> allLockerUnits() {
		ArrayList<LockerUnit> allUnits = new ArrayList<LockerUnit>();
		ArrayList<String> items = new ArrayList<String>();
		ArrayList<Integer> prices = new ArrayList<Integer>();
		int recID = 0;
		
		//Locker 150
		items.add("Imported Fireworks @ crate2");
		prices.add(1500);
		items.add("Antique Wooden Chair @ chair");
		prices.add(200);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit150, 900, items, prices, "Locker 150", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 151
		items.add("Oil Painting by Famous Artist @ frame");
		prices.add(700);
		items.add("Antique Lamp @ lamp");
		prices.add(100);		
		items.add("Box of Old Toys @ openbox");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit151, 500, items, prices, "Locker 151", LockerUnit.TIER_LOW));		
		prices = new ArrayList<Integer>();
		items = new ArrayList<String>();
				
		//Locker 152
		items.add("Rare Coins Found in Safe @ safe");
		prices.add(2000);
		items.add("Old Couch @ couch");
		prices.add(100);		
		items.add("Boxes of Old Clothes @ openbox");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit152, 700, items, prices, "Locker 152", LockerUnit.TIER_LOW));
		prices = new ArrayList<Integer>();
		items = new ArrayList<String>();
		
		//Locker 153
		items.add("Boxes of Old Sports Cards @ openbox");
		prices.add(800);
		items.add("Chrome car rim @ carrim");
		prices.add(50);
		items.add("Basketball @ basketball");
		prices.add(0);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit153, 400, items, prices, "Locker 153", LockerUnit.TIER_LOW));
		prices = new ArrayList<Integer>();
		items = new ArrayList<String>();
		
		//Locker 154
		items.add("9 Truck Tires @ tires");
		prices.add(700);
		items.add("Multiple Bags of Unopened Car Cleaning Supplies @ trashbag");
		prices.add(700);
		items.add("Toolbox Full of Tools @ toolbox");
		prices.add(300);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit154, 1200, items, prices, "Locker 154", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 155
		items.add("2 Boxes Containing Brand New Tall Gumball Machines @ tallbox2");
		prices.add(1500);
		items.add("2 Used Gumball Machines @ gumballmachine");
		prices.add(300);		
		items.add("2 Boxes Containing Multiple Bags of Fresh Gumballs @ openbox");
		prices.add(200);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit155, 1300, items, prices, "Locker 155", LockerUnit.TIER_MED)); 
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 156
		items.add("Empty Jewelry Chest  nightstand");
		prices.add(50);
		items.add("Old TV @ tv");
		prices.add(20);
		items.add("Bags of Tatterd Old Clothes @ trashbag");
		prices.add(0);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit156, 300, items, prices, "Locker 156", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 157
		items.add("Safe With Several Gold Coins @ goldcoin");
		prices.add(2000);
		items.add("2 Rare Vases @ vase");
		prices.add(500);
		items.add("2 Boxes of Fancy Glassware @ fragilebox");
		prices.add(400);
		items.add("Antique Lamp @ lamp");
		prices.add(100);
		items.add("Golf Clubs @ golfbag");
		prices.add(200);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit157, 1700, items, prices, "Locker 157", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 158
		items.add("2 Toolboxes of Cheaply Made Tools @ toolbox");
		prices.add(100);
		items.add("Worn Out Tires @ tires");
		prices.add(0);
		items.add("Broken Washing Machine @ washingmachine");
		prices.add(0);
		items.add("Antique Gas Station Sign @ gassign");
		prices.add(300);
		items.add("Bags of Old Clothes @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit158, 800, items, prices, "Locker 158", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
				
		//Locker 159
		items.add("Leather Couch @ couch");
		prices.add(400);
		items.add("4 Chairs for Dinning Room Set @ chair");
		prices.add(200);
		items.add("Empty Safe @ safe");
		prices.add(0);
		items.add("Broken Washing Machine @ washingmachine");
		prices.add(0);
		items.add("Bags of Old Clothes @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit159, 1000, items, prices, "Locker 159", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 160
		items.add("Old TV @ tv");
		prices.add(0);
		items.add("Boxes of DVD's @ openbox");
		prices.add(100);
		items.add("Bags of Old Toys @ trashbag");
		prices.add(0);
		items.add("Box Containing Fancy Dinner Plate Set @ fragilebox");
		prices.add(500);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit160, 300, items, prices, "Locker 160", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 161
		items.add("Rare Art Painting @ frame");
		prices.add(500);
		items.add("Antique Vase @ vase");
		prices.add(200);
		items.add("Washing Machine @ washingmachine");
		prices.add(100);
		items.add("Surfboard @ surfboard");
		prices.add(700);
		items.add("2 Boxes With Brand New Computers @ crate2");
		prices.add(1500);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit161, 2000, items, prices, "Locker 161", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 162
		items.add("Leather Couch @ couch");
		prices.add(400);
		items.add("Golf Clubs @ golfbag");
		prices.add(200);
		items.add("Toolbox With Good Quality Tools @ toolbox");
		prices.add(200);
		items.add("Lightly Used Tires @ tires");
		prices.add(200);
		items.add("Bags of Old Clothes @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit162, 700, items, prices, "Locker 162", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 163
		items.add("Safe with gold jewelry");
		prices.add(1000);
		items.add("Fancy glassware");
		prices.add(200);
		items.add("Brand new vertical fishtank");
		prices.add(300);
		items.add("Old TV");
		prices.add(0);
		items.add("Bags of old clothes");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit163, 900, items, prices, "Locker 163", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 164
		items.add("Box of Sports Memorabilia @ smallcrate");
		prices.add(800);
		items.add("Mountain Bike @ bike");
		prices.add(300);
		items.add("Surfboard @ surfboard");
		prices.add(200);
		items.add("Empty Toolbox @ toolbox");
		prices.add(0);
		items.add("Bags of Old Clothes @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit164, 1700, items, prices, "Locker 164", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 165
		items.add("Set of Chrome Wheels @ carrim");
		prices.add(700);
		items.add("Box With Car Speakers @ fragilebox");
		prices.add(400);
		items.add("Box of Random Car Parts @ openbox");
		prices.add(300);
		items.add("Empty Box @ openbox");
		prices.add(0);
		items.add("Worn Out Tires @ tires");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit165, 1000, items, prices, "Locker 165", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 166
		items.add("Safe With Gold Jewelry @ safe");
		prices.add(2000);
		items.add("Safe With Rare Coins @ safe");
		prices.add(1500);
		items.add("2 Boxes of Antiques @ smallcrate");
		prices.add(600);
		items.add("Mountain Bike @ bike");
		prices.add(300);
		items.add("Bags of Brand New Clothes With Tags @ trashbag");
		prices.add(300);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit166, 2500, items, prices, "Locker 166", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();		
		
		//locker 167
		items.add("Antique Lamp @ lamp");
		prices.add(100);
		items.add("Empty Jewelery Box @ nightstand");
		prices.add(50);
		items.add("2 Fake Antique Vases @ vase");
		prices.add(50);
		items.add("Dinner Room Chair @ chair");
		prices.add(50);
		items.add("Old Smelly Couch @ couch");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit167, 700, items, prices, "Locker 167", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 168
		items.add("Antique Gas Sign @ gassign");
		prices.add(500);
		items.add("New(ish) Washing Machine @ washingmachine");
		prices.add(500);
		items.add("2 Boxes of Football Cards @ openbox");
		prices.add(400);
		items.add("Antique Lamp @ lamp");
		prices.add(100);
		items.add("Bags of Old Clothes @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit168, 1100, items, prices, "Locker 168", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
				
		//locker 169
		items.add("Boxes of Brand New Teddy Bears @ teddybear");
		prices.add(300);
		items.add("Gumball Machine @ gumballmachine");
		prices.add(100);
		items.add("5 Brand New Basketballs @ basketball");
		prices.add(50);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit169, 600, items, prices, "Locker 169", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 170
		items.add("Jewlery Chest Full of Gold Jewelry @ nightstand");
		prices.add(1500);
		items.add("Pro Level Golf Clubs @ golfbag");
		prices.add(500);
		items.add("Top Quality Tools @ toolbox");
		prices.add(300);
		items.add("Rare Skateboard @ skateboard");
		prices.add(200);
		items.add("Worn Out Tires @ tires");
		prices.add(0);
		items.add("Bag of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit170, 900, items, prices, "Locker 170", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 171
		items.add("Empty Jewelry Chest @ nightstand");
		prices.add(50);
		items.add("Empty Safe @ safe");
		prices.add(50);
		items.add("Old Smelly Couch @ couch");
		prices.add(0);
		items.add("Broken Washing Machine @ washingmachine");
		prices.add(0);
		items.add("Old TV @ tv");
		prices.add(0);
		items.add("Bag of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit171, 700, items, prices, "Locker 171", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 172
		items.add("4 Boxes of Fancy Model Trains @ smallcrate");
		prices.add(1000);
		items.add("Boxes of Model Train Tracks @ openbox");
		prices.add(300);
		items.add("Bags of Model Train Tracks @ trashbag");
		prices.add(300);
		items.add("Antique Vase @ vase");
		prices.add(200);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit172, 900, items, prices, "Locker 172", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 173
		items.add("2 Fancy Mountain Bikes @ bike");
		prices.add(800);
		items.add("Tall Box With a Brand New Fish Tank @ tallbox2");
		prices.add(500);
		items.add("Nice Golf Clubs @ golfbag");
		prices.add(400);
		items.add("Toolbox With Quality Tools @ toolbox");
		prices.add(300);
		items.add("Box of Fancy Glassware @ fragilebox");
		prices.add(200);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit173, 1700, items, prices, "Locker 173", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 174
		items.add("Set of Chrome Wheels @ carrim");
		prices.add(700);
		items.add("Surfboard @ surfboard");
		prices.add(400);
		items.add("Leather Couch @ couch");
		prices.add(300);
		items.add("Antique Vase @ vase");
		prices.add(200);
		items.add("Old TV @ tv");
		allUnits.add(new LockerUnit(++recID, R.drawable.unit174, 1300, items, prices, "Locker 174", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 175
		items.add("Bags of Collector Teddy Bears @ trashbag");
		prices.add(800);
		items.add("Washer and Dryer Set @ washingmachine");
		prices.add(500);
		items.add("Antique Gas Sign @ gassign");
		prices.add(300);
		items.add("Rare Collectors Teddy Bear @ teddybear");
		prices.add(200);
		items.add("Boxes of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit175, 1400, items, prices, "Locker 175", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 176
		items.add("Multiple Boxes of Imported Cheap Plastic Toys @ smallcrate");
		prices.add(100);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit176, 900, items, prices, "Locker 176", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 177
		items.add("Safe Full of Gold Coins @ goldcoin");
		prices.add(3000);
		items.add("Safe Full of Diamond Jewelry @ safe");
		prices.add(2000);
		items.add("Safe Full of Rare Baseball Cards @ safe");
		prices.add(1000);
		items.add("Boxes of Antique Decorations @ openbox");
		prices.add(200);
		items.add("Several Worn Out Tires @ tires");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit177, 2500, items, prices, "Locker 177", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 178
		items.add("Original Painting by Famous Artist @ frame");
		prices.add(1200);
		items.add("Jewelry Box With Some Gold Jewelry Inside @ nightstand");
		prices.add(500);
		items.add("2 Boxes of Antique Plates and Glassware @ fragilebox");
		prices.add(400);
		items.add("4 Dining Chairs @ chair");
		prices.add(200);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit178, 1600, items, prices, "Locker 178", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 179
		items.add("Brand Name Guitar @ guitar");
		prices.add(500);
		items.add("Old Couch @ couch");
		prices.add(50);
		items.add("Used Gumball Machine @ gumballmachine");
		prices.add(50);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit179, 300, items, prices, "Locker 179", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 180
		items.add("Piggy Bank With Cash Inside @ piggybank");
		prices.add(300);
		items.add("Washing Machine @ washingmachine");
		prices.add(200);
		items.add("Brand new tires @ tires");
		prices.add(200);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		items.add("Box of Old Toys @ openbox");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit180, 400, items, prices, "Locker 180", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 181
		items.add("Samurai Sword @ sword");
		prices.add(200);
		items.add("Off-brand Golf Clubs @ golfbag");
		prices.add(100);
		items.add("2 Dining Room Chairs @ chair");
		prices.add(100);
		items.add("Old TV @ tv");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit181, 700, items, prices, "Locker 181", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 182
		items.add("Complete Drum Kit Boxed Up @ crate2");
		prices.add(400);
		items.add("Cheaply Made Guitar @ guitar");
		prices.add(100);
		items.add("Empty Safe @ Safe");
		prices.add(50);
		items.add("Boxes of Old Clothes @ openbox");
		prices.add(0);
		items.add("Bag of Paperwork @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit182, 1200, items, prices, "Locker 182", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
				//Locker 183
		items.add("Rare Skateboard @ skateboard");
		prices.add(250);
		items.add("Washing Machine @ Washing Machine");
		prices.add(200);
		items.add("Samurai Sword @ sword");
		prices.add(200);
		items.add("Empty Jewelry Chest @ nightstand");
		prices.add(50);
		items.add("Cheap Lamp @ lamp");
		prices.add(20);
		items.add("Box of Pots and Pans @ fragilebox");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit183, 1100, items, prices, "Locker 183", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 184
		items.add("2 Boxes of Old Baseball Cards @ openbox");
		prices.add(800);
		items.add("Toolbox With Quality Tools @ toolbox");
		prices.add(300);
		items.add("Nice Mountin Bike @ bike");
		prices.add(300);
		items.add("Antique Vase @ vase");
		prices.add(200);
		items.add("2 Old TVs @ tv");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit184, 800, items, prices, "Locker 184", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 185
		items.add("Safe With a Couple of Gold Coins @ goldcoin");
		prices.add(500);
		items.add("2 Boxes of Imported Fireworks @ crate2");
		prices.add(300);
		items.add("Tires With a lot of Tread Left @ tires");
		prices.add(300);
		items.add("Rare Collectors Teddy Bear @ teddybear");
		prices.add(200);
		items.add("Bags of trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit185, 1000, items, prices, "Locker 185", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 186
		items.add("2 Boxes of Scuba Gear @ openbox");
		prices.add(800);
		items.add("2 Surfboards in Great Shape @ surfboard");
		prices.add(600);
		items.add("Basketball With Players Signature on it @ basketball");
		prices.add(300);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit186, 700, items, prices, "Locker 186", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 187
		items.add("Box With Home Audio Speakers @ tallbox2");
		prices.add(200);
		items.add("2 Dinning Room Chairs @ chair");
		prices.add(100);
		items.add("Empty Piggy Bank @ piggybank");
		prices.add(0);
		items.add("Broken Lamp @ lamp");
		prices.add(0);
		items.add("Old Tatty Teddy Bear @ teddybear");
		prices.add(0);
		items.add("Bags of Old Broken Toys @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit187, 600, items, prices, "Locker 187", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 188
		items.add("Box of Nice Glassware @ fragilebox");
		prices.add(100);
		items.add("1 Chrome Rim @ carrim");
		prices.add(50);
		items.add("Printed Painting in a Cheap Frame @ frame");
		prices.add(50);
		items.add("Old Smelly Couch @ couch");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit188, 700, items, prices, "Locker 188", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 189
		items.add("Nice Guitar @ guitar");
		prices.add(300);
		items.add("2 Boxes of Classic Vinyl Records @ openbox");
		prices.add(300);
		items.add("Gumball Machine @ gumballmachine");
		prices.add(100);
		items.add("Old TV @ tv");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit189, 500, items, prices, "Locker 189", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
				
		//Locker 190
		items.add("8 Bags of Brand New Basketballs @ trashbag");
		prices.add(400);
		items.add("2 Basketballs With Famous Players Signatures @ basketball");
		prices.add(200);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit190, 200, items, prices, "Locker 190", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//locker 191
		items.add("Antique Gas Sign @ gassign");
		prices.add(300);
		items.add("2 Boxes of Sports Collectables @ openbox");
		prices.add(300);
		items.add("Leather Couch @ couch");
		prices.add(200);
		items.add("Samurai Sword @ sword");
		prices.add(200);
		items.add("Old TV @ tv");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit191, 700, items, prices, "Locker 191", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//locker 192
		items.add("Bags of Rare Unopened Toys @ trashbag");
		prices.add(600);
		items.add("3 Dinning Room Chairs @ chair");
		prices.add(150);
		items.add("Antique Lamp @ lamp");
		prices.add(100);		
		items.add("2 Boxes of Photo Albums @ smallcrate");
		prices.add(0);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit192, 500, items, prices, "Locker 192", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 193
		items.add("2 Boxes of Rare Sports Cards @ openbox");
		prices.add(200);
		items.add("Piggy Bank Full of Change @ piggybank");
		prices.add(150);
		items.add("Gumball Machine @ gumballmachine");
		prices.add(100);
		items.add("2 Boxes of Old Toys @ fragilebox");
		prices.add(50);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit193, 600, items, prices, "Locker 193", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 194
		items.add("Jewelry Chest With a Diamond Necklace @ nightstand");
		prices.add(700);
		items.add("Box of New Exercise Equipment @ tallbox2");
		prices.add(400);
		items.add("Washer and Dryer Machines @ washingmachine");
		prices.add(400);
		items.add("Old Tires @ tires");
		prices.add(0);
		items.add("Box of Cleaning Supplies @ openbox");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit194, 700, items, prices, "Locker 194", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 195
		items.add("3 Boxes of New Chrome Wheels @ crate2");
		prices.add(700);
		items.add("Toolbox With Nice Tools @ toolbox");
		prices.add(200);
		items.add("Box of New Car Parts @ @openbox");
		prices.add(200);
		items.add("2 Chrome Wheels @ carrim");
		prices.add(100);
		items.add("Old TV @ tv");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit195, 700, items, prices, "Locker 195", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 196
		items.add("Fancy Mountain Bike @ bike");
		prices.add(400);
		items.add("Boxes of New Electronics @ openbox");
		prices.add(300);
		items.add("New Skateboard @ skateboard");
		prices.add(100);
		items.add("Dinning Room Chair @ chair");
		prices.add(50);		
		items.add("Empty Piggy Bank @ piggybank");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit196, 400, items, prices, "Locker 196", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 197
		items.add("Safe With Autographed Sports Memorbilia @ safe");
		prices.add(700);
		items.add("Painting by Famous Artist @ frame");
		prices.add(500);
		items.add("Surfboard @ surfboard");
		prices.add(400);
		items.add("Set of fine gold clubs @ golfbag");
		prices.add(300);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit197, 1700, items, prices, "Locker 197", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 198
		items.add("Box With New Car Parts @ smallcrate");
		prices.add(500);
		items.add("Brand New Washing Machine @ washingmachine");
		prices.add(400);
		items.add("Antique Vase @ vase");
		prices.add(300);
		items.add("Piggy Bank With Some Change @ piggybank");
		prices.add(100);
		items.add("Bent Car Rim @ carrim");
		prices.add(0);
		items.add("Cheap Skateboard @ skateboard");
		prices.add(0);
		items.add("Box Full of Trash @ openbox");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit198, 1600, items, prices, "Locker 198", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
				
		//Locker 199
		items.add("2 Boxes of Crystal Glassware @ glassbox");
		prices.add(600);
		items.add("High End Guitar @ guitar");
		prices.add(500);
		items.add("2 Boxes of Brand New Computer Equipment @ crate2");
		prices.add(500);
		items.add("Rare Antique Lamp @ lamp");
		prices.add(400);
		items.add("3 High End Dinning Room Chairs @ chair");
		prices.add(300);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit199, 1500, items, prices, "Locker 199", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 200
		items.add("Jewelry Chest With Gold and Silver Jewelry @ nightstand");
		prices.add(700);
		items.add("Authentic Antique Samurai Sword @ Sword");
		prices.add(700);
		items.add("Safe Containing Old and Rare Coins @ safe");
		prices.add(500);
		items.add("Box With Rare War Memorabilia @ fragilebox");
		prices.add(500);
		items.add("Bags of Brand Name Clothes @ trashbag");
		prices.add(500);
		items.add("Box With Brand New Refrigerator @ tallbox2");
		prices.add(400);
		items.add("Toolbox With Decent Tools @ toolbox");
		prices.add(300);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit200, 2200, items, prices, "Locker 200", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 201
		items.add("Leather Couch @ couch");
		prices.add(400);
		items.add("Mountain Bike @ bike");
		prices.add(300);
		items.add("Rare Antique Gas Sign @ gassign");
		prices.add(300);
		items.add("Gumball Machine @ gumballmachine");
		prices.add(50);
		items.add("Empty Jewelry Box @ nightstand");
		prices.add(50);
		items.add("Old Tires @ tires");
		prices.add(0);
		items.add("2 Boxes of Paperwork @ openbox");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit201, 2000, items, prices, "Locker 201", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 202
		items.add("Safe Full of Gold Jewelry @ safe");
		prices.add(2000);
		items.add("Box With Brand New Arcade Machine @ tallbox2");
		prices.add(1800);
		items.add("Safe Full of Cash @ safe");
		prices.add(1500);
		items.add("Safe Containing Antique Firearms @ safe");
		prices.add(1500);
		items.add("2 Boxes of Imported Champagne @ crate2");
		prices.add(1000);
		items.add("Old Washing Machine @ washingmachine");
		prices.add(0);
		items.add("2 Bags of Old Clothes @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(+recID, R.drawable.unit202, 3300, items, prices, "Locker 202", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 203
		items.add("Original Painting @ frame");
		prices.add(800);
		items.add("2 Bags of Old Golf Clubs @ golfbag");
		prices.add(200);
		items.add("Box of Nice Glassware @ fragilebox");
		prices.add(200);
		items.add("Reproduction Vase @ vase");
		prices.add(0);
		items.add("Old TV @ tv");
		prices.add(0);
		items.add("2 Boxes of Broken Old Toys @ openbox");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit203, 1700, items, prices, "Locker 203", LockerUnit.TIER_HIGH));		
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 204
		items.add("Safe With Sports Memorabilia @ safe");
		prices.add(800);
		items.add("Nice Guitar @ guitar");
		prices.add(500);
		items.add("Samurai Sword @ sword");
		prices.add(400);
		items.add("Piggy Bank Full of Cash @ piggybank");
		prices.add(400);
		items.add("2 Boxes of Fireworks @ smallcrate");
		prices.add(400);
		items.add("Surfboard @ surfboard");
		prices.add(300);
		items.add("Toolbox With Nice Tools @ toolbox");
		prices.add(200);
		items.add("Antique Lamp @ lamp");
		prices.add(200);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit204, 2200, items, prices, "Locker 204", LockerUnit.TIER_HIGH));
		
		//Locker 300		
		items.add("2 Nice Barstools @ barstool");
		prices.add(100);
		items.add("Nice Glassware @ fragilebox");
		prices.add(100);
		items.add("Old smelly Refrigerator @ fridge");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit300, 400, items, prices, "Locker 300", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 301
		items.add("Leather Love Seat @ loveseat");
		prices.add(300);
		items.add("Nice Leather Briefcase @ briefcase");
		prices.add(100);
		items.add("Boxes With a lot of DVDs @ randombox");
		prices.add(200);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit301, 300, items, prices, "Locker 301", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 302
		items.add("Refrigerator in Good Condition @ fridge");
		prices.add(200);
		items.add("Nice Neon Sign @ neoncocktail");
		prices.add(200);
		items.add("New Washing Machine @ wshingmachine");
		prices.add(200);
		items.add("Old Couch @ couch");
		prices.add(100);
		items.add("Box of Old Broken Toys @ randombox");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit302, 500, items, prices, "Locker 302", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 303
		items.add("Signed Baseball Bat @ baseballbat");
		prices.add(250);
		items.add("Cheap Samurai Sword @ sword");
		prices.add(100);
		items.add("Small Flatscreen TV @ flatscreentv");
		prices.add(100);		
		items.add("Small Fish Tank @ fishtank");
		prices.add(0);
		items.add("Box of Paperwork @ stuffbox");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit303, 300, items, prices, "Locker 303", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 304
		items.add("Boxes of Old Video Games @ stuffbox");
		prices.add(700);
		items.add("Reprinted Artwork @ frame2");
		prices.add(150);
		items.add("Fake Casino Chips @ chips");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit304, 700, items, prices, "Locker 304", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 305
		items.add("Box of Rare Fishing Lures @ stuffbox");
		prices.add(500);
		items.add("Nice Refrigerator @ fridge");
		prices.add(400);		
		items.add("Pink Love Seat @ loveseat");
		prices.add(300);
		items.add("Bamboo Fishing Pole @ bamboofishingpole");
		prices.add(200);
		items.add("3 Nice Barstools @ barstool");
		prices.add(150);
		items.add("Bags of Clothes @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit305, 900, items, prices, "Locker 305", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 306
		items.add("Nice Poker Table @ pokertable");
		prices.add(300);
		items.add("High Quality Guitar @ guitar");
		prices.add(300);
		items.add("Mountain Bike @ bike");
		prices.add(250);
		items.add("Antique Bamboo Fishing Pole @ bamboofishingpole");
		prices.add(200);		
		items.add("Leather Briefcase @ briefcase");
		prices.add(50);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit306, 850, items, prices, "Locker 306", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 307
		items.add("Nightstand With Gold Jewerly Inside @ nightstand2");
		prices.add(500);
		items.add("Digital Safe With Cash Inside @ safe2");
		prices.add(500);
		items.add("Leather Couch @ couch");
		prices.add(300);
		items.add("Antique Lamp @ lamp");
		prices.add(100);		
		items.add("Boxes of Old Toys @ randombox");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit307, 800, items, prices, "Locker 307", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 308
		items.add("Medium Size Flat Panel TV @ flatscreentv");
		prices.add(300);
		items.add("Old Golf Clubs @ golfbag");
		prices.add(200);
		items.add("Box of Cheap Comic Books @ comicbooksbox");
		prices.add(100);
		items.add("Old Smelly Refrigerator @ fridge");
		prices.add(0);
		items.add("Box of Paperwork @ openbox");
		prices.add(0);
		items.add("Empty Toolbox @ toolbox");
		prices.add(0);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit308, 1400, items, prices, "Locker 308", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 309
		items.add("Safe With a Fancy Watch Inside @ safe");
		prices.add(700);
		items.add("Antique Gas Sign @ gassign");
		prices.add(400);
		items.add("Nice Artwork in Fancy Frame @ frame3");
		prices.add(300);
		items.add("Bamboo Fishing Pole @ bamboofishingpole");
		prices.add(300);
		items.add("Leather Love Seat @ loveseat");
		prices.add(300);		
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit309, 1300, items, prices, "Locker 309", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 310
		items.add("2 Nice Slot Machines @ slotmachine");
		prices.add(2500);
		items.add("Boxes of Antiques @ randombox");
		prices.add(500);
		items.add("Nice Barstool @ barstool");
		prices.add(100);		
		items.add("Bag of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit310, 1700, items, prices, "Locker 310", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 311
		items.add("Large Flat Panel TV @ flatscreentv");
		prices.add(500);
		items.add("Nice Poker Table @ pokertable");
		prices.add(300);
		items.add("Box of Fireworks @ crate2");
		prices.add(200);
		items.add("Fake Poker Chips @ chips");
		prices.add(0);		
		items.add("Box of Trash @ openbox");
		prices.add(0); 
		items.add("Bag of Trash @ trashbag");
		prices.add(0);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit311, 1900, items, prices, "Locker 311", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 312
		items.add("Box of Rare Comic Books @ comicbooksbox");
		prices.add(3000);
		items.add("Digital Safe With Diamond Jewelry @ safe2");
		prices.add(1500);
		items.add("Bags of Rare Toys @ trashbag");
		prices.add(500);
		items.add("Nice Washing Machine @ washingmachine");
		prices.add(300);		
		items.add("Bags of Rare Comic Books @ trashbag");
		prices.add(300);		
		items.add("Nice Mountain Bike @ bike");
		prices.add(300);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit312, 2000, items, prices, "Locker 312", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 313
		items.add("Antique Rifle @ rifle");
		prices.add(500);
		items.add("Nice Barstool @ barstool");
		prices.add(50);
		items.add("Small Fish Tank @ fishtank");
		prices.add(50);
		items.add("Broken Refrigerator @ fridge");
		prices.add(0);
		items.add("Empty Nightstand @ nightstand2");
		prices.add(0);
		items.add("Replica Vase @ vase");
		prices.add(0);
		items.add("Empty Piggy Bank @ piggybank");
		prices.add(0);		
		items.add("Broken Neon Sign @ neoncocktail");
		prices.add(0);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit313, 1600, items, prices, "Locker 313", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 314
		items.add("Fancy Slot Machine @ slotmachine");
		prices.add(800);		
		items.add("Designer Purse @ purse");
		prices.add(400);
		items.add("Bamboo Fishing Pole @ bamboofishingpole");
		prices.add(300);
		items.add("Pink Love Seat @ loveseat");
		prices.add(200);
		items.add("Reprinted Painting @ frame2");
		prices.add(100);
		items.add("Empty Guitar Case @ guitar");
		prices.add(50);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit314, 1800, items, prices, "Locker 314", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 315
		items.add("Jewelry Chest With Silver Jewelry @ nightstand");
		prices.add(300);
		items.add("Boxes of Clothes @ stuffbox");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		items.add("Autographed Basketball @ basketball");
		prices.add(300);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit315, 300, items, prices, "Locker 315", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 316
		items.add("Old Couch @ couch");
		prices.add(100);
		items.add("Nice Surfboard @ surfboard");
		prices.add(300);
		items.add("Antique Glassware @ smallcrate");
		prices.add(200);
		items.add("Box of Cheap Sports Cards @ sportscardsbox");
		prices.add(50);
		items.add("Nice Lamp @ lamp");
		prices.add(100);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit316, 600, items, prices, "Locker 316", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();	
		
		//locker 317
		items.add("Casino Poker Chips @ chips");
		prices.add(300);
		items.add("Broken Washing Machine @ washingmachine");
		prices.add(0);
		items.add("Old TV @ tv");
		prices.add(0);
		items.add("Empty Old Briefcase @ briefcase");
		prices.add(0);
		items.add("Box of Paperwork @ openbox");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit317, 700, items, prices, "Locekr 317", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 318
		items.add("Tool Box With Nice Tools @ toolbox");
		prices.add(300);
		items.add("Nice Tires @ tires");
		prices.add(200);		
		items.add("2 Boxes of DVDs @ stuffbox");
		prices.add(200);		
		items.add("Fake Casino Chips @ chips");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit318, 500, items, prices, "Locker 318", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 319
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		items.add("Box of Paperwork @ randombox");
		prices.add(0);
		items.add("2 Toy Footballs @ football");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit319, 350, items, prices, "Locker 319", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 320
		items.add("Nice Poker Table @ pokertable");
		prices.add(200);
		items.add("2 Nice Chairs @ chair");
		prices.add(200);
		items.add("Leather Briefcase @ briefcase");
		prices.add(50);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		items.add("Box of Old Electronics @ fragilebox");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit320, 900, items, prices, "Locker 320", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 321
		items.add("Nice Surfboard @ surfboard");
		prices.add(300);
		items.add("Nice Slot Machine @ slotmachine");
		prices.add(800);
		items.add("Leather Couch @ couch");
		prices.add(300);
		items.add("Box With Nice Laptop Inside @ randombox");
		prices.add(0);
		items.add("Box of Paperwork @ stuffbox");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit321, 1000, items, prices, "Locker 321", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 322
		items.add("Safe Full of Silver Jewelry @ safe");
		prices.add(700);
		items.add("Box With Vertical Fish Tank @ tallbox2");
		prices.add(400);
		items.add("Nice Refrigerator @ fridge");
		prices.add(300);		
		items.add("Small Flat Panel TV @ flatscreentv");
		prices.add(200);
		items.add("Nice Chair @ chair");
		prices.add(100);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit322, 1300, items, prices, "Locker 322", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 323
		items.add("Samurai Sword @ sword");
		prices.add(300);
		items.add("Autographed Baseball Bat @ baseballbat");
		prices.add(300);
		items.add("2 Boxes of Rare Sports Cards @ sportscardsbox");
		prices.add(700);
		items.add("Nice Mountain Bike @ bike");
		prices.add(300);
		items.add("Autographed Basketball @ basketball");
		prices.add(300);
		items.add("Broken Fish Tank @ fishtank");
		prices.add(0);
		items.add("Gumball Machine @ gumballmachine");
		prices.add(100);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit323, 1200, items, prices, "Locker 323", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 324
		items.add("Piggy Bank With Cash Inside @ piggybank");
		prices.add(300);
		items.add("Reprinted Artwork @ frame3");
		prices.add(100);
		items.add("Cheap Guitar @ guitar");
		prices.add(100);
		items.add("Old TV @ tv");
		prices.add(0);			
		items.add("Empty Digital Safe @ safe2");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit324, 1000, items, prices, "Locker 324", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 325
		items.add("Pink Love Seat @ lovesseat");
		prices.add(300);
		items.add("Safe Full of Jewelry @ safe");
		prices.add(1700);
		items.add("Empty Night Stand @ nightstand");
		prices.add(100);
		items.add("Antique Vase @ vase");
		prices.add(300);
		items.add("Old Rare Teddy Bear @ teddybear");
		prices.add(300);
		items.add("Antique Gun @ rifle");
		prices.add(400);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit325, 1700, items, prices, "Locker 325", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 326		
		items.add("Jewelry Chest With a Diamond Necklace @ nightstand");
		prices.add(1000);
		items.add("4 Bamboo Fishing Poles @ bamboofishingpole");
		prices.add(800);
		items.add("Casino Poker Chips @ chips");
		prices.add(300);
		items.add("Nice Washing Machine @ washingmachine");
		prices.add(300);
		items.add("Antique Glassware @ smallcrate");
		prices.add(200);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit326, 1800, items, prices, "Locker 326", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 327
		items.add("Large Flat Panel TV @ flatscreentv");
		prices.add(500);
		items.add("Surfboard @ surfboard");
		prices.add(300);
		items.add("Nice Barstool @ barstool");
		prices.add(100);
		items.add("Fake Designer Purse @ purse");
		prices.add(0);
		items.add("Broken Neon Sign @ neoncocktail");
		prices.add(0);	
		allUnits.add(new LockerUnit(++recID, R.drawable.unit327, 2000, items, prices, "Locker 327", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 328
		items.add("Box of Very Rare Sports Cards @ sportscardsbox");
		prices.add(1000);
		items.add("Fancy Slot Machine @ slotmachine");
		prices.add(800);
		items.add("Leather Love Seat @ loveseat");
		prices.add(400);
		items.add("Antique Rifle @ rifle");
		prices.add(400);
		items.add("Antique Gas Sign @ gassign");
		prices.add(300);
		items.add("Nice Poker Table @ pokertable");
		prices.add(200);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit328, 2200, items, prices, "Locker 328", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 329
		items.add("Painting by Famous Artist @ frame2");
		prices.add(1000);
		items.add("2 Boxes of Low Quality Comics @ comicbookbox");
		prices.add(300);
		items.add("Cheap Samurai Sword @ sword");
		prices.add(100);
		items.add("Empty Digital Safe @ safe2");
		prices.add(0);		
		items.add("Empty Guitar Case @ guitar");
		prices.add(0);		
		items.add("Box of Trash @ stuffbox");
		prices.add(0);
		items.add("Bag of Trash @ trashbag");
		prices.add(0);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit329, 1800, items, prices, "Locker 329", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 330
		items.add("2 Nice Barstools @ barstool");
		prices.add(100);
		items.add("Nice Skateboard @ skateboard");
		prices.add(100);
		items.add("Empty Piggy Bank @ piggybank");
		prices.add(0);
		items.add("Fish Tank With a Leak @ fishtank");
		prices.add(0);
		items.add("Old TV @ tv");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit330, 450, items, prices, "Locker 330", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 331
		items.add("Gumball Machine @ gumballmachine");
		prices.add(150);
		items.add("Chrome Car Rim @ carrim");
		prices.add(100);
		items.add("Boxes of Rare Video Games @ randombox");
		prices.add(0);
		items.add("Box of Old Toys @ stuffbox");
		prices.add(0);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit331, 300, items, prices, "Locker 331", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 332
		items.add("Bags of Brand New Basketballs @ basketball");
		prices.add(300);
		items.add("Bags of Brand New Footballs @ football");
		prices.add(300);
		items.add("Box of New Sports Equipment @ Randombox");
		prices.add(300);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit332, 500, items, prices, "Locker 332", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 333
		items.add("Old Smelly Couch @ couch");
		prices.add(0);
		items.add("Nice Fish Tank @ fishtank");
		prices.add(150);
		items.add("Old TV @ tv");
		prices.add(0);
		items.add("2 Good Chairs @ chair");
		prices.add(100);
		items.add("Bag of Clothes @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit333, 350, items, prices, "Locker 333", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 334
		items.add("4 Chrome Car Rims @ carrim");
		prices.add(400);
		items.add("Box of New Car Parts @ smallcrate");
		prices.add(300);
		items.add("Medium Flat Panel TV @ flatscreentv");
		prices.add(200);
		items.add("Broken Refrigerator @ fridge");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit334, 600, items, prices, "Locker 334", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 335
		items.add("Rare Teddy Bear @ teddybear");
		prices.add(100);
		items.add("Autographed Baseball Bat @ baseballbat");
		prices.add(300);
		items.add("Boxes of Sports Memorbilia @ stuffbox");
		prices.add(500);
		items.add("Tool Box With Cheap Tools @ toolbox");
		prices.add(150);
		items.add("Box of Paperwork @ stuffbox");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit335, 550, items, prices, "Locker 335", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 336
		items.add("Piggy Bank With Cash @ piggybank");
		prices.add(200);
		items.add("Neon Cocktail Sign @ neoncocktail");
		prices.add(200);
		items.add("Nice Skateboard @ skateboard");
		prices.add(100);
		items.add("Empty Safe @ safe");
		prices.add(0);		
		items.add("Old Worn Out Tires @ tires");
		prices.add(0);		
		items.add("Box of Old Casettes @ randombox");
		prices.add(0);
		items.add("Bag of Clothes @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit336, 700, items, prices, "Locker 336", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 337
		items.add("Leather Couch @ couch");
		prices.add(300);
		items.add("Designer Purse @ purse");
		prices.add(400);
		items.add("Nice Washing Machine @ washingmachine");
		prices.add(200);
		items.add("Small Flat Panel TV @ flatscreentv");
		prices.add(100);
		items.add("Box of Paperwork @ stuffbox");
		prices.add(0);
		items.add("Bags of Clothes @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit337, 400, items, prices, "Locker 337", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 338		
		items.add("Digital Safe With Gold Necklace @ safe2");
		prices.add(500);
		items.add("Painting by Famous Artist @ frame");
		prices.add(500);
		items.add("Box of Rare Comic Books @ comicbooksbox");
		prices.add(500);
		items.add("Pink Love Seat @ loveseat");
		prices.add(300);
		items.add("Box of DVDs @ stuffbox");
		prices.add(100);
		items.add("Empty Nightstand @ nightstand2");
		prices.add(50);
		items.add("Old TV @ tv");
		prices.add(0);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit338, 900, items, prices, "Locker 338", LockerUnit.TIER_LOW));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 339
		items.add("Antique Rifle @ rifle");
		prices.add(300);
		items.add("Leather Sofa @ couch");
		prices.add(200);
		items.add("Old Golf Clubs @ golfbag");
		prices.add(100);
		items.add("Nice Chair @ chair");
		prices.add(50);
		items.add("Empty Safe @ safe");
		prices.add(0);
		items.add("Boxes of Broken Glassware @ smallcrate");
		prices.add(0);				
		allUnits.add(new LockerUnit(++recID, R.drawable.unit339, 1100, items, prices, "Locker 339", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 340
		items.add("Briefcase With a Nice Watch Inside @ briefcase");
		prices.add(500);
		items.add("Nice Poker Table @ pokertable");
		prices.add(300);		
		items.add("Real Casino Chips @ chips");
		prices.add(300);
		items.add("Antique Vase @ vase");
		prices.add(300);
		items.add("Box of Fancy Dinnerware @ fragilebox");
		prices.add(150);
		items.add("Bags of Old Toys @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit340, 1200, items, prices, "Locker 340", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 341
		items.add("Boxes of Imported Electronics @ crate2");
		prices.add(1000);
		items.add("Nice Tires @ tires");
		prices.add(300);
		items.add("Fancy Mountain Bike @ bike");
		prices.add(300);
		items.add("Box of Cheap Comics @ comicbooksbox");
		prices.add(100);
		items.add("Bags of Clothes @ trashbag");
		prices.add(0);		
		items.add("Broken Flat Panel TV @ flatscreentv");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit341, 800, items, prices, "Locker 341", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//locker 342
		items.add("A lot of Nice New Tires @ tires");
		prices.add(700);
		items.add("Chrome Rim @ carrim");
		prices.add(100);
		items.add("Nice Barstool @ barstool");
		prices.add(50);		
		items.add("Empty Tool Box @ toolbox");
		prices.add(50);
		items.add("Bags of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit342, 1300, items, prices, "Locker 342", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//locker 343
		items.add("Safe With Gold Jewelry Inside @ safe");
		prices.add(1000);
		items.add("Large Flat Panel TV @ flatscreentv");
		prices.add(400);
		items.add("Box of Old Sports Cards @ sportscardsbox");
		prices.add(400);
		items.add("Leather Love Seat @ loveseat");
		prices.add(300);		
		items.add("Antique Lamp @ lamp");
		prices.add(200);
		items.add("Nice Skateboard @ skateboard");
		prices.add(100);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit343, 1700, items, prices, "Locker 343", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 344
		items.add("Fancy Slot Machine @ slotmachine");
		prices.add(800);
		items.add("Antique Rifle @ rifle");
		prices.add(400);		
		items.add("Reproduction Painting @ frame2");
		prices.add(100);
		items.add("Box of DVDs @ fragilebox");
		prices.add(100);
		items.add("Empty Nightstand @ nightstand");
		prices.add(50);
		items.add("Empty Piggy Bank @ piggybank");
		prices.add(0);		
		items.add("Bag of Clothes @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit344, 2000, items, prices, "Locker 344", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		//Locker 345
		items.add("Boxes of Rare Sports Cards @ sportscardsbox");
		prices.add(1000);
		items.add("Digital Safe With Cash @ safe2");
		prices.add(1000);
		items.add("2 Autographed Baseball Bats @ baseballbat");
		prices.add(600);
		items.add("Nice Refrigerator @ fridge");
		prices.add(400);
		items.add("Pink Love Seat @ loveseat");
		prices.add(300);		
		items.add("Autographed Football @ football");
		prices.add(200);		
		items.add("Box of paperwork @ stuffbox");
		prices.add(0);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit345, 2100, items, prices, "Locker 345", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 346
		items.add("Safe With Silver Jewelry @ safe");
		prices.add(300);
		items.add("Nice Guitar @ guitar");
		prices.add(300);		
		items.add("Real Casino Chips @ chips");
		prices.add(300);
		items.add("Bamboo Fishing Pole @ bamboofishingpole");
		prices.add(200);
		items.add("Empty Cheap Briefcase @ briefcase");
		prices.add(0);
		items.add("Broken Neon Sign @ neoncocktail");
		prices.add(0);
		items.add("Box of Clothes @ randombox");
		prices.add(0);
		items.add("Bag of Trash @ trashbag");
		prices.add(0);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit346, 1800, items, prices, "Locker 346", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 347
		items.add("Fancy Slot Machine @ slotmachine");
		prices.add(800);		
		items.add("Jewelry Chest With Diamond Ring @ nightstand");
		prices.add(700); 
		items.add("Designer Purse @ purse");
		prices.add(500);
		items.add("Box With Brand New Vertical Fish Tank @ fishtank");
		prices.add(400);
		items.add("Antique Gas Sign @ gassign");
		prices.add(300);
		items.add("Broken Gumball Machine @ gumballmachine");
		prices.add(0);
		items.add("Bags of Clothes @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit347, 1600, items, prices, "Locker 347", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 348
		items.add("Digital Safe With Gold Jewelry @ safe2");
		prices.add(1000);
		items.add("Nice Poker Table @ pokertable");
		prices.add(300);
		items.add("Surfboard @ surfboard");
		prices.add(300);		
		items.add("Nice Skateboard @ skateboard");
		prices.add(100);
		items.add("Cheap Samurai Sword @ sword");
		prices.add(100);
		items.add("Bag of Trash @ trashbag");
		prices.add(0);
		items.add("Fish Tank With Leak @ fishtank");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit348, 348, items, prices, "Locker 348", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 349
		items.add("2 Paintings by Famous Artist @ frame1");
		prices.add(2000);
		items.add("Nice Washing Machine @ washingmachine");
		prices.add(300);
		items.add("Boxes of Rare Comic Books @ comicbooksbox");
		prices.add(800);
		items.add("Piggy Bank With Cash @ piggybank");
		prices.add(200);
		items.add("Nice Barstool @ barstool");
		prices.add(0);
		items.add("Box of Old Electronics @ randombox");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit349, 2150, items, prices, "Locker 349", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 350
		items.add("2 Fancy Slot Machines @ slotmachine");
		prices.add(1600);
		items.add("2 Boxes With Brand New Slot Machines @ tallbox2");
		prices.add(2000);
		items.add("Real Casino Chips @ chips");
		prices.add(300);
		items.add("Box of Slot Machine Parts @ smallcrate");
		prices.add(200);
		items.add("Nice Chair @ chair");
		prices.add(50);
		items.add("Bag of Trash @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit350, 3000, items, prices, "Locker 350", LockerUnit.TIER_HIGH));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 351
		items.add("Nice Golf Clubs @ golfbag");
		prices.add(300);		
		items.add("Antique Lamp @ lamp");
		prices.add(200);
		items.add("Gumball Machine @ gumballmachine");
		prices.add(100);
		items.add("Empty Leather Briefcase @ briefcase");
		prices.add(100);
		items.add("Box of DVDs @ openbox");
		prices.add(100);
		items.add("Bags of Clothes @ trashbag");
		prices.add(0);
		items.add("Broken Refrigerator @ fridge");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit351, 1000, items, prices, "Locker 351", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 352		
		items.add("Jewelry Chest With Diamond Earrings @ nightstand");
		prices.add(500);
		items.add("Antique Rifle @ rifle");
		prices.add(400);
		items.add("Nice Poker Table @ pokertable");
		prices.add(300);
		items.add("Digital Safe With Cash @ safe2");
		prices.add(300);		
		items.add("Toolbox With Nice Tools @ toolbox");
		prices.add(300);
		items.add("Box of New Video Games @ stuffbox");
		prices.add(300);
		items.add("Nice Barstool @ barstool");
		prices.add(50);
		items.add("Bag of clothes @ trashbag");
		prices.add(0);		
		allUnits.add(new LockerUnit(++recID, R.drawable.unit352, 1300, items, prices, "Locker 352", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
		
		//Locker 353
		items.add("Fancy Slot Machine @ slotmachine");
		prices.add(800);
		items.add("Nice Mountain Bike @ bike");
		prices.add(300);
		items.add("Samurai Sword @ sword");
		prices.add(300);
		items.add("Nice Fish Tank @ fishtank");
		prices.add(100);	
		items.add("Box of Cheap Comic Books @ comicbooksbox");
		prices.add(100);
		items.add("Cheap Teddy Bear @ teddybear");
		prices.add(0);
		items.add("Reproduction Vase @ vase");
		prices.add(0);	
		items.add("Bag of Old Toys @ trashbag");
		prices.add(0);
		allUnits.add(new LockerUnit(++recID, R.drawable.unit353, 1250, items, prices, "Locker 353", LockerUnit.TIER_MED));
		items = new ArrayList<String>();
		prices = new ArrayList<Integer>();
			
		return allUnits;
	}
	
	public static class HistoryObject {
		
		String date, title, out, in, profit;
		
		public HistoryObject(String pDate, String pTitle, String pIn, String pOut) {
			date = pDate;
			title = pTitle;
			out = pOut;
			in = pIn;
			
			//work out the profit (out - in)
			int iProfit = (Integer.parseInt(pIn) - Integer.parseInt(pOut));
			profit = Integer.toString(iProfit);
		}
		
	}
}

