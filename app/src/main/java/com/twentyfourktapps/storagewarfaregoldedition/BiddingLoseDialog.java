package com.twentyfourktapps.storagewarfaregoldedition;

import java.util.Random;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class BiddingLoseDialog extends Dialog{

	Context context;
	boolean isReplay, isLastLocker;
	int curTier;
	int numOfViewsLeft;
	final int MAX_MONTHS = 12;
	String historyDate;
	
	int curBidder, curQuote;
	String[] quotes;
	
	public BiddingLoseDialog(Context pContext, int theme, boolean toRepeat, int lockerTier, int viewsLeft, int rival, String hDate) {
		super(pContext, theme);
		context = pContext;
		isReplay = toRepeat;
		curTier = lockerTier;
		numOfViewsLeft = viewsLeft;		
		curBidder = rival;
		historyDate = hDate;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bidding_lose_layout);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		isLastLocker = false;	
		PlayerObject player = new PlayerObject(context);
		if(player.monthsPlayed >= MAX_MONTHS) isLastLocker = true;
		setButtonListeners();
		
		
		//Select a rival bidder
		Random rand = new Random(System.currentTimeMillis());
		int[] imageIDs = { R.drawable.marc_smiling, R.drawable.terry_smiling, R.drawable.jenny_smiling, R.drawable.nick_smiling };
		//Select a quote
		curQuote = rand.nextInt(quotes.length);
		
		//Set the image and quotation
		final TextView quoteTV = (TextView)findViewById(R.id.biddinglose_textview_quote);
		final ImageView charIV = (ImageView)findViewById(R.id.biddinglose_imageview_character);
		
		quoteTV.setText(quotes[curQuote]);
		charIV.setImageResource(imageIDs[curBidder]);
		
	}
	
	public void setQuoteArray(String[] array){
		quotes = array;
	}
	private void setButtonListeners() {
		Button nextButton = (Button)findViewById(R.id.biddinglose_button_next);
		nextButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				dismiss();				
				Intent mIntent;
				if (isReplay) {
					 mIntent = new Intent(context, DestinationScreen.class);
					 mIntent.putExtra("tier", curTier);
					 mIntent.putExtra("attempts", numOfViewsLeft);
					 mIntent.putExtra("date", historyDate);
				} else if (isLastLocker){
					 mIntent = new Intent(context, GameOverActivity.class);
				} else {
					mIntent = new Intent(context, HomeScreen.class);
				}
				
				mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				context.startActivity(mIntent);
			}
		});
	}
}

