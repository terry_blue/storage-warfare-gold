package com.twentyfourktapps.storagewarfaregoldedition;

import java.util.ArrayList;


public class LockerUnit {

	public final static int TIER_LOW = 1;
	public final static int TIER_MED = 2;
	public final static int TIER_HIGH = 3;
	
	int unitID;
	int imageResourceID;
	String lockerTitle;
	int lockerTier;
	ArrayList<String> itemNameArrayList;
	ArrayList<Integer> itemPriceArrayList;
	int lockerValue;
	int winningBid;
	boolean isUsed;
	
	
	
	public LockerUnit(int _id, int _resId, int _winBid, ArrayList<String> _items, ArrayList<Integer> _prices, String _title, int _tier) {
		unitID = _id;
		imageResourceID = _resId;
		winningBid = _winBid;
		lockerTitle = _title;
		itemNameArrayList = _items;
		itemPriceArrayList = _prices;
		lockerTier = _tier;
		
		lockerValue = 0;
		for(int i = 0; i < itemPriceArrayList.size(); i++) {
			lockerValue += itemPriceArrayList.get(i);
		}
	}
	
	
	
	
	
	
}

