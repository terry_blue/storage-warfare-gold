package com.twentyfourktapps.storagewarfaregoldedition;

import java.lang.ref.WeakReference;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.twentyfourktapps.storagewarfaregoldedition.LockerUnitManager.HistoryObject;



public class DestinationScreen extends Activity implements DialogInterface.OnDismissListener{

	Context context;
	GameSounds gameSounds;
	LockerUnit curLockerUnit;
	boolean isWrapupAvailable;
	String historyDate;
	int lockersLeftToView;
	int rivalChoice = -1;
	int[] rivalImageIDs = { R.drawable.marc_yelling, R.drawable.terry_yelling, R.drawable.jenny_yelling, R.drawable.nick_yelling };
	String[] bidPhrases, marcBidPhrases, terryBidPhrases, jennyBidPhrases, nickBidPhrases;
	
	boolean isPredestinationDone = true;
	boolean isOnWrapupScreen = false;
	boolean isOnLoseScreen = false;
	int usersBidAmount = 0;
	
	final String BUNDLE_KEY_PREDEST = "predestinationScreen";
	final String BUNDLE_KEY_WRAPUP = "wrapup";
	final String BUNDLE_KEY_LOSE = "lose";
	final String BUNDLE_KEY_RIVAL = "rival";
	final String BUNDLE_KEY_CUR_LOCKER = "locker";
	final String BUNDLE_KEY_CUR_BID = "bid";

	BiddingEndHandler biddingEndHandler;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.destinationscreen_layout);
		context = this;
		
		biddingEndHandler = new BiddingEndHandler(this);
		
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		gameSounds = new GameSounds(context);
		
		historyDate = getIntent().getExtras().getString("date");
		isWrapupAvailable = true;
		setupRival();
		
		
		//Choose a locker to display
		chooseLockerToDisplay();
		
		//Set the background image of the locker 
		LinearLayout imageLayout = (LinearLayout)findViewById(R.id.destinationscreen_layout_mainimage_layout);
		imageLayout.setBackgroundResource(curLockerUnit.imageResourceID);		
		
		setListeners();
		setDebugViews();
		
		boolean toShowPreD = true;
		boolean toShowWrapup = false;
		boolean toShowLoseDialog = false;
		if(savedInstanceState != null) {
			if(savedInstanceState.containsKey(BUNDLE_KEY_PREDEST)) {
				toShowPreD = !(savedInstanceState.getBoolean(BUNDLE_KEY_PREDEST));
			}
			if(savedInstanceState.containsKey(BUNDLE_KEY_WRAPUP)){
				toShowWrapup = savedInstanceState.getBoolean(BUNDLE_KEY_WRAPUP);
			}
			if(savedInstanceState.containsKey(BUNDLE_KEY_LOSE)) {
				toShowLoseDialog = savedInstanceState.getBoolean(BUNDLE_KEY_LOSE);
			}
		}
		if (toShowPreD) {
			final PreDestinationDialog predestinationDialog = new PreDestinationDialog(context, R.style.predestination_layout);
			predestinationDialog.setOnDismissListener(this);
			predestinationDialog.show();
			Button predestinationUnlockButton = (Button)predestinationDialog.findViewById(R.id.predestinationscreen_button_unlock);
			predestinationUnlockButton.setOnClickListener(new View.OnClickListener() {
				
				public void onClick(View v) {
					gameSounds.playDoorOpen();
					predestinationDialog.dismiss();
				}
			});
		}
		
		else if(toShowWrapup) {
			isOnWrapupScreen = true;
			WrapupDialog wrapupDialog = new WrapupDialog(context, R.style.wrapup_layout, curLockerUnit.unitID, (lockersLeftToView > 0), curLockerUnit.lockerTier, lockersLeftToView, historyDate);
			wrapupDialog.show();
		}
		
		else if(toShowLoseDialog) {
			isOnLoseScreen = true;
			BiddingLoseDialog loseDialog = new BiddingLoseDialog(context, R.style.wrapup_layout_lose, (lockersLeftToView > 0), curLockerUnit.lockerTier, lockersLeftToView, rivalChoice, historyDate);
			Resources res = getResources();		
			if(isWrapupAvailable) {
				String[] trashTalkArray = (rivalChoice == 3) ? res.getStringArray(R.array.nick_trashtalk_array): res.getStringArray(R.array.trashtalk_array);
				loseDialog.setQuoteArray(trashTalkArray);
				isOnLoseScreen = true;
				loseDialog.show();		
			}
		}
		
	}


	private void setupRival() {
		Random r = new Random(System.currentTimeMillis());
		rivalChoice = (r.nextInt(9999) % 4);
		
		marcBidPhrases = getResources().getStringArray(R.array.marc_bids);
		terryBidPhrases = getResources().getStringArray(R.array.terry_bids);
		jennyBidPhrases = getResources().getStringArray(R.array.jenny_bids);
		nickBidPhrases = getResources().getStringArray(R.array.nick_bids);
		
		switch(rivalChoice) {
		case 0: {
			bidPhrases = marcBidPhrases;
			break;
		}
		case 1: {
			bidPhrases = terryBidPhrases;
			break;
		}
		case 2: {
			bidPhrases = jennyBidPhrases;
			break;
		}
		case 3: {
			bidPhrases = nickBidPhrases;
			break;
		}
		default: {
			bidPhrases = jennyBidPhrases;
		}
		}
	}


	private void chooseLockerToDisplay() {
		LockerUnitManager luM = new LockerUnitManager();
		Intent intent = getIntent();
		curLockerUnit = luM.getNewUnit(intent.getExtras().getInt("tier"), context);
		lockersLeftToView = (intent.getExtras().getInt("attempts")) - 1;
	}


	private void setListeners() { 
		
		PlayerObject player = new PlayerObject(context);		
		final TextView playersCashTV = (TextView)findViewById(R.id.destinationscreen_textview_players_total_crash);
		playersCashTV.setText(Integer.toString(player.currentScore));
		
		final Button startButton = (Button)findViewById(R.id.destinationscreen_button_startbidding);		
		startButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				//startButton.setEnabled(false);
				if (verifyUserBid()) startBiddingProcess();	
			}
		});
		
		final Button walkButton = (Button)findViewById(R.id.destinationscreen_button_walk);
		walkButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				gameSounds.playAww();
				final DatabaseInterface DI = new DatabaseInterface(context);
				DI.addHistoryObject(new HistoryObject(historyDate, curLockerUnit.lockerTitle, "0", "0"));
				
						
				BiddingLoseDialog loseDialog = new BiddingLoseDialog(context, R.style.wrapup_layout_lose, (lockersLeftToView > 0), curLockerUnit.lockerTier, lockersLeftToView, rivalChoice, historyDate);
				Resources res = getResources();		
				if(isWrapupAvailable) {
					loseDialog.setQuoteArray(res.getStringArray(R.array.trashtalk_array));
					isOnLoseScreen = true;
					loseDialog.show();		
				}
			}
		});
		
		final EditText userBidEditText = (EditText)findViewById(R.id.destinationscreen_edittext_bidamount);
		userBidEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_UNSPECIFIED || actionId == EditorInfo.IME_ACTION_NONE || actionId == EditorInfo.IME_ACTION_SEARCH) {					
					return !verifyUserBid();
				}
				return false;
			}
		});
		
		userBidEditText.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				userBidEditText.setText("");	
			}
		});
		
		
	}
	private boolean verifyUserBid() {
		PlayerObject player = new PlayerObject(context);
		final EditText userBidET = (EditText)findViewById(R.id.destinationscreen_edittext_bidamount);
		String amountString = userBidET.getText().toString();
		if(amountString.equals("")) amountString = "0";		
		usersBidAmount = (int)Float.parseFloat(amountString);
		
		if(amountString.contains(".")) {
			Toast.makeText(context, "Come on, this is the big time, we don't deal in cents!", Toast.LENGTH_SHORT).show();
			userBidET.setText(Integer.toString(usersBidAmount));
			return false;
		}
		
		
		//get the current winning bid amount
		TextView curBidTV = (TextView)findViewById(R.id.destinationscreen_textview_current_winning_bid);
		int curBid = Integer.parseInt(curBidTV.getText().toString());
		
		if (usersBidAmount > player.currentScore) {
			//Player does not have enough cash to make that bid
			Toast toast = Toast.makeText(context, "You have only $" + Integer.toString(player.currentScore) + ", make a lower bid.", Toast.LENGTH_SHORT);
			toast.show();			
			return false;
		}
		if (usersBidAmount <= curBid && curBid > 0) {
			//Players bid is lower or equal to the current winning bid
			Toast toast = Toast.makeText(context, "The bid currently stands at $" + Integer.toString(curBid) + ". Place a higher bid", Toast.LENGTH_SHORT);
			toast.show();
			return false;
		}
		
		return true;
	}
	private void startBiddingProcess() {
		BiddingThread biddingThread = new BiddingThread();
		biddingThread.start();
		
		//Set the rival bidder image view
		ImageView rivalBidderIV = (ImageView)findViewById(R.id.destinationscreen_imageview_rival_bidder);
		rivalBidderIV.setImageResource(rivalImageIDs[rivalChoice]);		
		
		//Set the rival bidder text
		TextView rivalBidderTextPhrase = (TextView)findViewById(R.id.destinationscreen_textview_rival_bid);
		Random rand = new Random();
		rivalBidderTextPhrase.setText(bidPhrases[rand.nextInt(9999) % bidPhrases.length]);
		
		//If the user is to be outbid, show a rival bid
		//EditText usersBidEditText = (EditText)findViewById(R.id.destinationscreen_edittext_bidamount);
		//int usersBidAmount = Integer.parseInt(usersBidEditText.getText().toString());
		//if(usersBidAmount < curLockerUnit.winningBid) {
			
		//}
	}
	
	private static class BiddingEndHandler extends Handler {
		final private WeakReference<DestinationScreen> mTarget;
		public BiddingEndHandler(DestinationScreen target) {
			mTarget = new WeakReference<DestinationScreen>(target);
		}
		@Override
		public void handleMessage(Message msg) {
			DestinationScreen thisTarget = mTarget.get();
			//decide if user should have another go at bidding or go to wrap up screen.
			final EditText bidEditText = (EditText)thisTarget.findViewById(R.id.destinationscreen_edittext_bidamount);
			TextView curWinningBidTV = (TextView)thisTarget.findViewById(R.id.destinationscreen_textview_current_winning_bid);
			//int curWinningBid = Integer.parseInt(curWinningBidTV.getText().toString());
			final int USER_BID_AMOUNT = Integer.parseInt(bidEditText.getText().toString());
			final PlayerObject player = new PlayerObject(thisTarget.context);
			
			if (msg.obj.equals("bid_round_complete")) {				
				//DEBUG
				ViewFlipper viewFlipper = (ViewFlipper)thisTarget.findViewById(R.id.destinationscreen_viewflipper_bottom);
				viewFlipper.setDisplayedChild(0);
				
				if(USER_BID_AMOUNT >= thisTarget.curLockerUnit.winningBid) { //if the user has entered a winning bid
					
					//Work out the users profit/loss on this locker and add it to the players DB entry
					int userProfit = thisTarget.curLockerUnit.lockerValue - USER_BID_AMOUNT;
					
					player.currentScore += userProfit;
					player.SaveFields();
					
					//Save the locker unit to the history table
					final DatabaseInterface DI = new DatabaseInterface(thisTarget.context);
					DI.addHistoryObject(new HistoryObject(thisTarget.historyDate, thisTarget.curLockerUnit.lockerTitle, Integer.toString(thisTarget.curLockerUnit.lockerValue), Integer.toString(USER_BID_AMOUNT)));
					
					if(thisTarget.isWrapupAvailable) {
						thisTarget.gameSounds.playApplause();
						thisTarget.isOnWrapupScreen = true;
						WrapupDialog wrapupDialog = new WrapupDialog(thisTarget.context, R.style.wrapup_layout, thisTarget.curLockerUnit.unitID, (thisTarget.lockersLeftToView > 0), thisTarget.curLockerUnit.lockerTier, thisTarget.lockersLeftToView, thisTarget.historyDate);
						wrapupDialog.show();
						thisTarget.isWrapupAvailable = false;
					}
				} 
				
			} else if(msg.obj.equals("outbid_user")) {
				//Take the players current bid and add an amount, dependent on the tier of locker (currently defaulted to $50) - *round it to the next 50*
				int newWinningBid = USER_BID_AMOUNT;
				do newWinningBid++; while (!(newWinningBid % 50 == 0));
				curWinningBidTV.setText(Integer.toString(newWinningBid));
				
				//set the Edit Text to the next bid
				if(player.currentScore >= (newWinningBid + 50)) bidEditText.setText(Integer.toString(newWinningBid + 50));
				
				//re-enable the bid button
				final Button bidButton = (Button)thisTarget.findViewById(R.id.destinationscreen_button_startbidding);
				bidButton.setEnabled(true);
				ViewFlipper viewFlipper = (ViewFlipper)thisTarget.findViewById(R.id.destinationscreen_viewflipper_bottom);
				viewFlipper.setDisplayedChild(0);
				
			} 	else if(msg.obj.equals("show_rival")) {
				//Show rival bar
				ViewFlipper viewFlipper = (ViewFlipper)thisTarget.findViewById(R.id.destinationscreen_viewflipper_bottom);
				viewFlipper.setDisplayedChild(1);
				if (thisTarget.rivalChoice == 2) thisTarget.gameSounds.playFemaleYeah();
				else if(thisTarget.rivalChoice == 3) thisTarget.gameSounds.playNickYeah();
				else thisTarget.gameSounds.playYeah();
			}
		}
	};
	
	private void setDebugViews() {
		/*
		TextView winningBidAmountTV = (TextView)findViewById(R.id.destinationscreen_textview_winningbidamount);
		TextView lockerValueAmountTV = (TextView)findViewById(R.id.destinationscreen_textview_lockervalueamount);
		
		winningBidAmountTV.setText(Integer.toString(curLockerUnit.winningBid));
		lockerValueAmountTV.setText(Integer.toString(curLockerUnit.lockerValue));
		*/
	}
	
	private class BiddingThread extends Thread {
		
		@Override
		public void run() {
			try {				
				//determine if the player should be outbid
				EditText playersBidET = (EditText)findViewById(R.id.destinationscreen_edittext_bidamount);
				int playersBid = Integer.parseInt(playersBidET.getText().toString());
				int winningBid = curLockerUnit.winningBid;
				//Log.e("Destination Screen", "playersBid = " + Integer.toString(playersBid) + " winning bid = " + Integer.toString(winningBid));
				if(playersBid < winningBid) {
					Message mssg = new Message();
					mssg.obj = "show_rival";
					biddingEndHandler.sendMessage(mssg);
					sleep(1000);
					
					Message msg = new Message();
					msg.obj = "outbid_user";
					biddingEndHandler.sendMessage(msg);
					
					//Log.e("Destination screen", "outbid user msg sent");
				}	else {			
					String string = "bid_round_complete";
					Message msg = new Message();
					msg.obj = string;
					biddingEndHandler.sendMessage(msg);
					//Log.e("Destination screen", "bid round complete msg sent");
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		savedInstanceState.putBoolean(BUNDLE_KEY_PREDEST, isPredestinationDone);
		savedInstanceState.putBoolean(BUNDLE_KEY_WRAPUP, isOnWrapupScreen);
		savedInstanceState.putBoolean(BUNDLE_KEY_LOSE, isOnLoseScreen);
		savedInstanceState.putInt(BUNDLE_KEY_RIVAL, rivalChoice);
		savedInstanceState.putInt(BUNDLE_KEY_CUR_LOCKER, curLockerUnit.unitID);
		savedInstanceState.putInt(BUNDLE_KEY_CUR_BID, usersBidAmount);
		
		super.onSaveInstanceState(savedInstanceState);
	}
	
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		
		usersBidAmount = savedInstanceState.getInt(BUNDLE_KEY_CUR_BID);
		rivalChoice = savedInstanceState.getInt(BUNDLE_KEY_RIVAL);
		switch(rivalChoice) {
		case 0: {
			bidPhrases = marcBidPhrases;
			break;
		}
		case 1: {
			bidPhrases = terryBidPhrases;
			break;
		}
		case 2: {
			bidPhrases = jennyBidPhrases;
			break;
		}
		case 3: { 
			bidPhrases = nickBidPhrases;
			break;
		}
		default: {
			bidPhrases = jennyBidPhrases;
		}
		}
		
		int lockerUnitIDChoice = savedInstanceState.getInt(BUNDLE_KEY_CUR_LOCKER);
		LockerUnitManager luM = new LockerUnitManager();
		curLockerUnit = luM.getLockerNumber(lockerUnitIDChoice);
		
		
	}
	
	@Override
	public void onDismiss(DialogInterface arg0) {
		isPredestinationDone = true;
		
	}

	

}
