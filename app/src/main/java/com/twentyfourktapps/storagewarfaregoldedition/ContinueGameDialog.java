package com.twentyfourktapps.storagewarfaregoldedition;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class ContinueGameDialog extends Dialog {

	Context context;
	GameSounds gameSounds;
	
	public ContinueGameDialog(Context pContext, int theme) {
		super(pContext, theme);
		context = pContext;
		gameSounds = new GameSounds(context);
		
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.continuegamedialog_layout);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setButtonListeners();
		
		
	}
	
	public void setName(String newName) {
		TextView nameTV = (TextView)findViewById(R.id.continuegamedialog_textview_name);
		nameTV.setText(newName);
	}
	
	private void setButtonListeners() {
		Button continueGameButton = (Button)this.findViewById(R.id.continuegamedialog_button_continue);
		Button newGameButton = (Button)this.findViewById(R.id.continuegamedialog_button_new);
	
		continueGameButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				gameSounds.playStorageWarfareYeah();
				
				StartGameThread startGameThread = new StartGameThread();
				startGameThread.start();
				
			}
		});
		
		newGameButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				new AlertDialog.Builder(context)
		        .setIcon(android.R.drawable.ic_dialog_alert)
		        .setTitle("Confirm")
		        .setMessage("Are you sure you want to start again?")
		        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		
		            
		            public void onClick(DialogInterface dialog, int which) {
		
		            	//delete the history
						DatabaseInterface DI = new DatabaseInterface(context);
						DI.clearHistory();
						
						PlayerObject playerObject = new PlayerObject(context);
						playerObject.Reset();	
						playerObject.close();
						dismiss();   
		            }
		
		        })
		        .setNegativeButton("No", null)
		        .show();
				
				
			}
		});
	}

	Handler startGameHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			context.startActivity(new Intent(context, HomeScreen.class));				
			dismiss();
		}
	};
	
	private class StartGameThread extends Thread {
		
		@Override
		public void run() {
			//try { sleep(500); } catch(Exception e) { }
			
			Message msg = new Message();
			msg.obj = "start_game";
			startGameHandler.sendMessage(msg);
		}
	}
}
