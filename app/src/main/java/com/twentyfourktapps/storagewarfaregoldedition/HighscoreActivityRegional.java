package com.twentyfourktapps.storagewarfaregoldedition;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;


public class HighscoreActivityRegional extends HighscoreAbstract implements LocationListener{

	ArrayList<HighscoreObject> highscoreObjects;
	LinearLayout table;
	
	LocationManager locationManager;
	Geocoder geocoder;
	
	String rankString;
	String regionString;
	String scoreString;
	
	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.highscore_activity_regional);
		table = new LinearLayout(context);
		table.setOrientation(LinearLayout.VERTICAL);
		
		locationManager = (LocationManager)context.getSystemService(LOCATION_SERVICE);
		geocoder = new Geocoder(context);
		
		Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		
		if(location != null){
			this.onLocationChanged(location);
		}
		
		Receiver receiver = new Receiver();
		receiver.start();
	}
	
	Handler tableBuilt = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if(msg.obj.equals("build_complete")) {
				//remove the progress spinner and insert the table
				LinearLayout tableRootLayout = (LinearLayout)findViewById(R.id.highscore_activity_regional_linearlayout);
				tableRootLayout.removeAllViews();
				tableRootLayout.addView(table);
				createTable();
			} 
		}
	};
	
	private void createTable() {
		TextView positionTV = (TextView)findViewById(R.id.highscore_activity_regional_position_column);
		int posColumnWidth = positionTV.getWidth();
		
		//Set the player specific data
		TextView rankTV = (TextView)findViewById(R.id.highscore_activity_regional_rank);
		rankTV.setText(rankString);
		TextView scoreTV = (TextView)findViewById(R.id.highscore_activity_regional_textview_score);
		scoreTV.setText(scoreString);
		TextView countryTV = (TextView)findViewById(R.id.highscore_activity_regional_textview_region);
		countryTV.setText(regionString);
		
		//Set the highscore table data
		for(int i = 0; i < highscoreObjects.size(); i++) {
			LinearLayout row = new LinearLayout(context);
			LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, 1);
			
			TextView posTV = new TextView(context);
			posTV.setText(Integer.toString(i + 1));
			posTV.setWidth(posColumnWidth);
			row.addView(posTV);
			
			TextView nameTV = new TextView(context);
			nameTV.setText(highscoreObjects.get(i).name);
			row.addView(nameTV, params);
			
			TextView cashInTV = new TextView(context);
			cashInTV.setText(Integer.toString(highscoreObjects.get(i).totalIn));
			row.addView(cashInTV, params);
			
			TextView cashOutTV = new TextView(context);
			cashOutTV.setText(Integer.toString(highscoreObjects.get(i).totalOut));
			row.addView(cashOutTV, params);
			
			TextView endScoreTV = new TextView(context);
			endScoreTV.setText(Integer.toString(highscoreObjects.get(i).endScore));
			row.addView(endScoreTV, params);
			
			table.addView(row);
		}
				
	}
	
	private class Receiver extends Thread{
		
		
		
		public Receiver() {
			
		}
		
		@Override
		public void run() {
			UUID_Manager uuid_m = new UUID_Manager(context);
			
			GlobalHighscoreReciever ghsr = new GlobalHighscoreReciever(context, uuid_m.getUUID(), "regional", regionString);
			highscoreObjects = ghsr.getHighscoreObjects();
			rankString = ghsr.getUsersRank();
			scoreString = ghsr.getUsersHighestScore();
			
			if(rankString.equals("-1")) rankString = "Unranked";
			if(scoreString.equals("-1")) scoreString = "None";
			
			//Send message back to original thread
			Message msg = new Message();
			msg.obj = "build_complete";
			tableBuilt.sendMessage(msg);
		}		
	}

	@Override
	public void onLocationChanged(Location location) {
		try {
			List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
			for(Address address : addresses) {
				regionString = address.getCountryName();
			}
			Toast toast = Toast.makeText(context, regionString, Toast.LENGTH_LONG);
			toast.show();
		} catch (Exception e) {
			Log.e("Highscore Screen", e.toString());
		}
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onBackPressed() {
		Intent mIntent = new Intent(context, TitleScreen.class);
		mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(mIntent);	
	}

}
