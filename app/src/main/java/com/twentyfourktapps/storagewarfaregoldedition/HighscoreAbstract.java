package com.twentyfourktapps.storagewarfaregoldedition;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.TableLayout;

public abstract class HighscoreAbstract extends Activity {

	Context context;
	TableLayout tableLayout;
	
	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		
	}
}