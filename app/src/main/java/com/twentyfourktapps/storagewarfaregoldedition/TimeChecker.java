//This file is used to store a start and end time for each application
//The times are then used to work out how long a user has spent in an 
//app. The time difference (Unix) is then sent to the server for storage.


//Written April 2012 by Terry White

package com.twentyfourktapps.storagewarfaregoldedition;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.util.Log;

public class TimeChecker {

	Context context;
	
	final String urlDestination = "http://www.drunkzombies.com/Services/time/store_time.php";
	final String START_TIME_FILENAME = "start_time";
	final String END_TIME_FILENAME = "end_time";
	
	HttpClient httpClient;
	HttpPost httpPost;
	ArrayList<NameValuePair> nameValuePairs;
	private boolean isDataSet;
	
	String appName = "";
	private long startTime = 0;
	private long endTime = 0;
	
	public TimeChecker(Context pContext, String applicationName) {
		context = pContext;
		appName = applicationName;
		
		isDataSet = false;
	}
	
	public void setStartTime() {
		startTime = System.currentTimeMillis();
		String startTimeString = Long.toString(startTime);

		//Store start time string
		try {
			FileOutputStream fOs = context.openFileOutput(START_TIME_FILENAME, Context.MODE_PRIVATE);
			fOs.write(startTimeString.getBytes());
			fOs.close();
		} catch (Exception e) {
			Log.e("TimeChecker", "Storing start time error: " + e.toString());
		}
	}
	public long getStartTime() {
		return startTime;
	}
	
	public void setEndTime() {
		endTime = System.currentTimeMillis();
		String endTimeString = Long.toString(endTime);

		//Store end time string
		try {
			FileOutputStream fOs = context.openFileOutput(END_TIME_FILENAME, Context.MODE_PRIVATE);
			fOs.write(endTimeString.getBytes());
			fOs.close();
		} catch (Exception e) {
			Log.e("TimeChecker", "Storing end time error: " + e.toString());
		}
	}
	public long getEndTime() {
		return endTime;
	}
	
	public void setDataForSever() {
		httpClient = new DefaultHttpClient();
		httpPost = new HttpPost(urlDestination);
		nameValuePairs = new ArrayList<NameValuePair>(2);
		
		StringBuilder sb  = new StringBuilder();
		String startTimeString = "";
		String endTimeString = "";
		
		File startTimeFile = context.getFileStreamPath(START_TIME_FILENAME);
		File endTimeFile = context.getFileStreamPath(END_TIME_FILENAME);
		
		if(startTimeFile.exists() && endTimeFile.exists()) {
			try {
				FileInputStream fIs = new FileInputStream(startTimeFile);
				int i = 0;
				while((i = fIs.read()) != -1) {
					sb.append((char)i);
				}
				startTimeString = sb.toString();
				startTimeFile.delete();
			} catch (Exception e) {
				Log.e("TimeChecker error", "error in startTimeFile: " + e.toString());
			}
			
			sb = new StringBuilder();
			
			try {
				FileInputStream fIs = new FileInputStream(endTimeFile);
				int i = 0;
				while((i = fIs.read()) != -1) {
					sb.append((char)i);
				}
				endTimeString = sb.toString();
				endTimeFile.delete();
			} catch (Exception e) {
				Log.e("TimeChecker error", "error in endTimeFile: " + e.toString());
			}
			
			if(!(startTimeString.equals("")) && !(endTimeString.equals(""))) {
				UUID_Manager uuid_m = new UUID_Manager(context);
				long timeDifference = Long.parseLong(endTimeString) - Long.parseLong(startTimeString);
				nameValuePairs.add(new BasicNameValuePair("uuid", uuid_m.getUUID()));
				nameValuePairs.add(new BasicNameValuePair("app_title", appName));
				nameValuePairs.add(new BasicNameValuePair("time", Long.toString(timeDifference)));
				isDataSet = true;
			}
			
		}
	}
	
	public void sendDataToServer() {
		
		if (isDataSet) {
			try {
				httpClient = new DefaultHttpClient();
				httpPost = new HttpPost(urlDestination);
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				httpClient.execute(httpPost);	
			} catch (Exception e) {
				Log.e("TimeChecker http error", e.toString());
			}
		}
	}
	
	
}
