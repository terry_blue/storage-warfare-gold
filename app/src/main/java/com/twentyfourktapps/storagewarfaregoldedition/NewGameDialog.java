package com.twentyfourktapps.storagewarfaregoldedition;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class NewGameDialog extends Dialog {

	Context context;
	GameSounds gameSounds;
	
	public NewGameDialog(Context pContext, int theme) {
		super(pContext, theme);
		context = pContext;
		gameSounds = new GameSounds(context);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.newgamedialog_layout);
		
		
		setButtonListeners();
	}
	
	private void setButtonListeners() {
		Button startGameButton = (Button)findViewById(R.id.newgamedialog_button_start);
		Button closeDialogButton = (Button)findViewById(R.id.newgamedialog_button_close);
		final EditText nameEditText = (EditText)findViewById(R.id.newgamedialog_edittext_name);
		
		startGameButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				
				String name = nameEditText.getText().toString();
				PlayerObject playerObject = new PlayerObject(context);
				
				//Save the name to the player database, creating a new record in the process
				
				
				if(playerObject.CreateNew(name)) {
					gameSounds.playStorageWarfareYeah();
					context.startActivity(new Intent(context, HomeScreen.class));
					dismiss();
				}
				playerObject.close();
			}
		});
		
		
		nameEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_UNSPECIFIED || actionId == EditorInfo.IME_ACTION_NONE || actionId == EditorInfo.IME_ACTION_SEARCH) {					
					return !verifyName();
				}
				return false;
			}
		});
		
		closeDialogButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				dismiss();				
			}
		});
	}

	public boolean verifyName() {
		
		final EditText nameEditText = (EditText)findViewById(R.id.newgamedialog_edittext_name);
		String uName = nameEditText.getText().toString();
		if(uName.equals("")) {
			Toast.makeText(context, "Please enter a name", Toast.LENGTH_SHORT).show();
			return false;
		}
		Pattern p = Pattern.compile("^[A-Za-z0-9 ]+$");
		Matcher m = p.matcher(uName);
		if(!m.find()) {
			Toast.makeText(context, "Text and numbers only", Toast.LENGTH_SHORT).show();
			return false;
		}
		if(uName.length() > 8) {
			Toast.makeText(context, "8 Characters max", Toast.LENGTH_SHORT).show();
			nameEditText.setText(uName.substring(0, 8));
			return false;
		}
		
		return true;
	}
}
