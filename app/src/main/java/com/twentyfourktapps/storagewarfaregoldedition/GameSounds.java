package com.twentyfourktapps.storagewarfaregoldedition;

import java.util.Random;

import android.content.Context;

public class GameSounds extends SoundManager{

	final int APPLAUSE = 1;
	final int AWW = 2;
	final int DOOR_OPEN = 3;
	final int CLICK = 4;
	final int YEAH = 5;
	final int YEAH_FEMALE = 6;
	
	final int NICK_YEAH_1 = 7;
	final int NICK_YEAH_2 = 8;
	final int NICK_YEAH_3 = 9;
	final int NICK_YEAH_4 = 10;
	final int NICK_YEAH_5 = 11;
	final int NICK_YEAH_6 = 12;
	
	final int STORAGE_WARFARE_YEAH = 13;
	
	
	public GameSounds (Context pContext) {
		super(pContext);
		
		initSounds();
	}
	
	public void initSounds() {
		super.initializeSound(STORAGE_WARFARE_YEAH, R.raw.storeagewarfare_yeah, 1);
		super.initializeSound(APPLAUSE, R.raw.applause, 1);
		super.initializeSound(AWW, R.raw.awww, 1);
		super.initializeSound(DOOR_OPEN, R.raw.dooropen, 1);
		super.initializeSound(CLICK, R.raw.mouseclick, 1);
		super.initializeSound(YEAH, R.raw.yeah, 1);
		super.initializeSound(YEAH_FEMALE, R.raw.yeahfemale, 1);
		super.initializeSound(NICK_YEAH_1, R.raw.nick_sound1, 1);
		super.initializeSound(NICK_YEAH_2, R.raw.nick_sound2, 1);
		super.initializeSound(NICK_YEAH_3, R.raw.nick_sound3, 1);
		super.initializeSound(NICK_YEAH_4, R.raw.nick_sound4, 1);
		super.initializeSound(NICK_YEAH_5, R.raw.nick_sound5, 1);
		super.initializeSound(NICK_YEAH_6, R.raw.nick_sound6, 1);
		
	}
	
	public void playApplause() {
		super.playSound(APPLAUSE);
	}
	public void playAww() {
		super.playSound(AWW);
	}
	public void playClick() {
		super.playSound(CLICK);
	}
	public void playDoorOpen() {
		super.playSound(DOOR_OPEN);
	}
	public void playYeah() {
		super.playSound(YEAH);
	}
	public void playFemaleYeah() {
		super.playSound(YEAH_FEMALE);
	}
	
	public void playNickYeah() {
		Random rand = new Random(System.currentTimeMillis());
		int[] nickYeahs = { NICK_YEAH_1, NICK_YEAH_2, NICK_YEAH_3, NICK_YEAH_4, NICK_YEAH_5, NICK_YEAH_6 };
		int choice = rand.nextInt(9999) % nickYeahs.length;
		super.playSound(nickYeahs[choice]);
	}
	
	public void playStorageWarfareYeah() {
		super.playSound(STORAGE_WARFARE_YEAH);
		//Log.e("GAME SOUNDS", "Storage Warfare Yeah played");
	}
}
