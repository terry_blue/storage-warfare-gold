package com.twentyfourktapps.storagewarfaregoldedition;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.util.Log;

public class UUID_Manager{

	Context context;
	
	final private String FILENAME = "UUID_file";
	private String UUID = "-1";
	
	HttpClient httpClient;
	HttpPost httpPost;
	InputStream inputStream = null;
	String tempResult = "";
	String result = "";
	final String GET_URL = "http://drunkzombies.com/Services/get_uuid.php";
	
	public UUID_Manager(Context pContext) {
		//Determine if app already has a UUID
		context = pContext;
		File file = pContext.getFileStreamPath(FILENAME);
		if(file.exists()) {
			//Get and assign the UUID
			StringBuilder sb = new StringBuilder();
			try {
				FileInputStream fIS = new FileInputStream(file);
				int i = 0;
				while((i = fIS.read()) != -1) {
					sb.append((char)i);
				}
				UUID = sb.toString();
				Log.i("UUID_Manager UUID number", UUID);
				fIS.close();
				
			} catch (Exception e) {
				Log.e("UUID_Manager", e.toString());
			}
			
		} else {
			//Retrieve a response from server (new UUID)
			try {
				httpClient =  new DefaultHttpClient();
				httpPost = new HttpPost(GET_URL);
				HttpResponse response = httpClient.execute(httpPost);
				HttpEntity entity = response.getEntity();
				inputStream = entity.getContent();
			} catch (Exception e) {
				Log.e("UUID_Manager HTTP error", e.toString());
			}
			
			//Convert the response
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 32);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line);
					Log.i("RESULT", line);
				}
				inputStream.close();
				UUID = sb.toString();
				
				
				//Save the result to internal storage
				FileOutputStream foS = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
				foS.write(UUID.getBytes());
				foS.close();
				
				
				Log.i("UUID_Manager new UUID result", UUID);
			} catch (Exception e) {
				Log.e("UUI_Manager response convertor", e.toString());
			}
		}
	}
	
	public String getUUID() {
		return UUID;
	}
	
}
