package com.twentyfourktapps.storagewarfaregoldedition;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;



public class Splash extends Activity {
	
	Context context;	
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen_layout);
        context = this;
        
        UUID_Manager uuid = new UUID_Manager(context);
        uuid.getUUID();
        
        final PackageManager pm = context.getPackageManager();
        ApplicationInfo ai;
        try {
        	ai = pm.getApplicationInfo("com.twentyfourktapps.storagewarfare", 0);
        } catch (Exception e) {
        	ai = null;
        	Log.e("Splash", "getApplicationInfo error: " + e.toString());
        }        
        final String APPLICATION_NAME = (String)(ai != null ? pm.getApplicationLabel(ai) : "(unknown)");
        
        TimeChecker timeChecker = new TimeChecker(context, APPLICATION_NAME);
        timeChecker.setStartTime();
        
        /** create a thread to show splash up to splash time */
        Thread welcomeThread = new Thread() {
        	
	        @Override
	        public void run() {
	        	
		        	
		        String response = ServerUtil.getResponse(ServerUtil.WEB_PREFIX);
		        Log.e("SPLASH", "response = " + response);
		        if(response.equals("fail")) {
		        	
		        } else {
		        	startActivity(new Intent(getApplicationContext(), TitleScreen.class));
		        	finish();
		        }      	
		    }	
        };
        
        
        Thread loadDBThread = new Thread() {
        	
        	@Override
        	public void run() {
        		try {
        			super.run();
        			
        			
        		} catch (Exception e) {
        			Log.e("LOAD DB THREAD ERROR", e.toString());
        		}
        	}
        };
        
        welcomeThread.start();
        loadDBThread.start();
    }


	
}