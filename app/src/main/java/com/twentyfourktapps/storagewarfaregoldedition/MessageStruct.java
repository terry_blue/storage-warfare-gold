package com.twentyfourktapps.storagewarfaregoldedition;

public class MessageStruct {

	private String message;
	private String character;
	private int messageID;
	
	public MessageStruct(String pMessage, String pChar, int pMessageID) {
		message = pMessage;
		character = pChar;
		messageID = pMessageID;
	}	
	public String getMessage() {
		return message;
	}
	public String getCharacter() {
		return character;
	}
	public int getMessageID () {
		return messageID;
	}
}
