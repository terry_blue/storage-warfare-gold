package com.twentyfourktapps.storagewarfaregoldedition;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ViewAnimator;

public class InstructionsActivity extends Activity {

	Button prevButton;
	Button nextButton;
	ViewAnimator viewAnimator;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.instructionsactivity_layout);
		
		createViews();
		setButtonListeners();
	}
	
	private void createViews() {
		prevButton = (Button)findViewById(R.id.instructionsactivity_button_prev);
		nextButton = (Button)findViewById(R.id.instructionsactivity_button_next);
		viewAnimator = (ViewAnimator)findViewById(R.id.instructionsactivity_viewanimator);
	}
	
	private void setButtonListeners() {
		prevButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (viewAnimator.getDisplayedChild() != 0) {
					viewAnimator.showPrevious();			
					setEnabledButtons();
				}				
			}
		});
		
		nextButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (viewAnimator.getDisplayedChild() < viewAnimator.getChildCount()) {
					viewAnimator.showNext(); 
					setEnabledButtons();
				}
				
			}
		});
		
		setEnabledButtons();
	}
	
	private void setEnabledButtons() {
		int curView = viewAnimator.getDisplayedChild();
		
		if(curView <= 0) {
			prevButton.setVisibility(View.INVISIBLE);
		} else if (curView == (viewAnimator.getChildCount() -1)) {
			nextButton.setVisibility(View.INVISIBLE);
		} else {
			prevButton.setVisibility(View.VISIBLE);
			nextButton.setVisibility(View.VISIBLE);
		}
	}
}

